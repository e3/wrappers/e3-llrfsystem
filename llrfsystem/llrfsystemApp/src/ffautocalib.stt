program ffautocalib
/*Compilation options*/
option +r; /*Make re-entrant*/
option +s; /*Make safe*/

short enable;

double new_egu,  new_raw;
short proc_new_pt;

double mag, mag_rb;

int stats_cur_size, stats_max_size;
short stats_reset, stats_enable;
double stats_result;


double start, end, smpnm;
double cur_mag, step;

int pulse_count, last_pulse_count;

short egu_rst, raw_rst;

short cal_enable;

assign enable to "{P}{R}FFAutoCalEn";

assign new_egu to "{P}{R}FFCalNewEGU";
assign new_raw to "{P}{R}FFCalNewRaw";
assign proc_new_pt to "{P}{R}#FFCalNewVal.PROC";

assign mag to "{P}{R}FFCal-Mag";
assign mag_rb to "{PD}{RD}RFCtrlCnstFF-Mag-RB";

assign stats_cur_size to "{P}{R}FFCalStatsLst.NORD";
assign stats_max_size to "{P}{R}FFCalStatsSmpNm";
assign stats_reset to "{P}{R}FFCalStatsRst";
assign stats_result to "{P}{R}FFCalStats";
assign stats_enable to "{P}{R}FFCalStatsEn";

assign start to "{P}{R}FFAutoCalStr";
assign end to "{P}{R}FFAutoCalEnd";
assign smpnm to "{P}{R}FFAutoCalSmpNm";

assign pulse_count to "{PD}{RD}PulseDoneCnt";

assign egu_rst to "{P}{R}FFCalEGURst";
assign raw_rst to "{P}{R}FFCalRawRst";

assign cal_enable to "{PD}{RD}RFCtrlFF-CalEn";

monitor enable;
monitor stats_cur_size;
monitor stats_max_size;
monitor pulse_count;
monitor mag_rb;

ss ffcalib {
    state init_state {
        when (enable == TRUE){
            //reset auto add
            pvGet(start);
            pvGet(end);
            pvGet(smpnm);
            cur_mag = start;
            step = (end - start)/smpnm;

            //reset egu/raw
            egu_rst = TRUE;
            pvPut(egu_rst, SYNC);
            raw_rst = TRUE;
            pvPut(raw_rst, SYNC);

            //disable calibration
            cal_enable = FALSE;
            pvPut(cal_enable, SYNC);

            //set first mag val
            mag = cur_mag;
            pvPut(mag, SYNC);
            last_pulse_count = pulse_count;

        }state wait_mag

    }
    //this state is to wait the magnitude arrive on the right value
    //then enable statistical
    state wait_mag{
        when (pulse_count > last_pulse_count) {
            //reset stats
            stats_reset = TRUE;
            pvPut(stats_reset, SYNC);

            //enable stats
            stats_enable = TRUE;
            pvPut(stats_enable);
        }state monitor_stats
       when (enable == FALSE) {
       }state init_state
    }
    state monitor_stats {
       when (enable == FALSE) {
       }state init_state
       when(enable == TRUE && stats_cur_size == stats_max_size) {
            //Disable stats
            stats_enable = FALSE;
            pvPut(stats_enable, SYNC);
        }state auto_insert
    }
    state auto_insert {
        when (enable == FALSE) {
        }state init_state

        when (delay(0.1)){
                pvGet(stats_result);
                new_egu = stats_result;
                pvPut(new_egu, SYNC);

                new_raw = mag_rb;
                pvPut(new_raw, SYNC);
                proc_new_pt = 1;
                pvPut(proc_new_pt, SYNC);
        } state next_point
    }

    state next_point {
       when (enable == FALSE) {
       }state init_state

        when () {
            //Go to next magnitude point
            cur_mag = cur_mag + step;
            mag = cur_mag;
            pvPut(mag, SYNC);

            //save to check later
            last_pulse_count = pulse_count;
        }state check_if_finish
    }

    state check_if_finish {
        //check if finish auto calib
        //TODO if finish, sinalize
        when(cur_mag + step > end) {
            // disable auto add and stats
            enable = FALSE;
            pvPut(enable, SYNC);

            stats_enable = FALSE;
            pvPut(stats_enable);
        }state init_state

        when(cur_mag + step <= end) {
        }state wait_mag
    }
}
