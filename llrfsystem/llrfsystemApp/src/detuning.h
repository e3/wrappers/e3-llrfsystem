#include <aSubRecord.h>

#define INVALID_ARRAY_SIZE -1;
#define RANGE_ERROR        -2;

#define NO_VALID_SAMPLE    -1;
/**
 *
 * @brief: Calculate simple linear regression y(x) = a*x + b
 *
 *double dt,
        double* data, size_t Nsamples,
        double *offset, double *slope
 * @param [in]  dt                  time between samples
 * @param [in]  data                input data
 * @param [in]  Nsample             size of data array
 * @param [out] offset              y-offset b
 * @param [out] slope               slope a
 * method (Hz)
 *
 * Fits a linear function y(x) = a*x + b to the input data,
 *  sampled with dt spacing, using standard linear regression
 */
void linear_regression();

void extract_and_unwrap_phase(
    int i_start, size_t Nsamples,
    double* data_magnitude, double* data_phase,
    double* phases,
    double threshold_magnitude, double threshold_angle);

void calc_detuning_decay(int i_start,
        int i_end,double dt, double* data_magnitude,
        double* data_phase, size_t Nin,
        double threshold_magnitude, double threshold_angle,
        double* detuning);

void calc_detuning_phase_shift(
        int i_start, int i_end,
        double dt,
        double* cavity_magnitude,  double* cavity_phase,
        double* forward_magnitude, double* forward_phase,
        size_t Nin,
        double threshold_magnitude, double threshold_angle,
        double phase_offset, double Ql,
        double frequency_nominal,
        double* detuning);


static long detuning_calculate_phase_shift(aSubRecord *psr);

/*
 * Calculate detuning using decay method
 *
 * Parameters:
 *  INA: Phase waveform from LLRF
 *  INB: Magnitude waveform from LLRF
 *  INC: End of the pulse in INA/INB (in us)
 *  IND: Start of the fitting window after INC (in us)
 *  INE: Length of the fitting window (in us)
 *  INF: Sampling frequency from LLRF (LLRF:F-SAMPLING)
 *  ING: Near-IQ N from LLRF (LLRF:NEARIQN)
 *  INH: Threshold in phase of phase wrap-around detection (expert setting)
 *  INI: Threshold in magnitude of phase wrap-around detection (expert setting)
 *  INJ: Enable debug output on IOC shell (expert setting)
 *
 * Output:
 *  VALA: Detune value
 *  VALB: Quality factor
 */
static long detuning_calculate(aSubRecord *psr);

/*
 * Calculate the moving average
 *  INPA: Value to average over
 *  INPB: Number of points Navg to average over
 *
 * Output:
 *  VALA: Moving average
 *  VALB: Last Navg points
 *  VALC: Actual number of points in array
 *    (needs to be tracked, since EPICS insists
 *     on initialising arrays with one element)
 */
static long detuning_moving_average(aSubRecord *psr);
