#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <aSubRecord.h>

#include "detuning.h"

/**
 *
 * @brief: Calculate simple linear regression y(x) = a*x + b
 *
 *double dt,
        double* data, size_t Nsamples,
        double *offset, double *slope
 * @param [in]  dt                  time between samples
 * @param [in]  data                input data
 * @param [in]  Nsample             size of data array
 * @param [out] offset              y-offset b
 * @param [out] slope               slope a
 * method (Hz)
 *
 * Fits a linear function y(x) = a*x + b to the input data,
 *  sampled with dt spacing, using standard linear regression
 */
void linear_regression(
        double dt,
        double* data, size_t Nsamples,
        double *offset, double *slope)
{
    // Sums for linear regression
    double sum = 0, sumi = 0;
    unsigned i;
    for(i = 0; i < Nsamples; ++i) {
        sum  += data[i];
        sumi += data[i]*i;
    }

    // Average time: half of the sampling window
    double const t_avg = 0.5*(Nsamples-1)*dt;
    // Average phase in window
    double const data_avg = sum / Nsamples;
    // Calculates linear regression:
    //   sum (t_i - t_avg)*(phase_i - phase_avg) / sum(t_i - t_avg)^2
    // Denominator: sum(t_i - t_avg)^2
    double norm  = dt*dt*Nsamples*(Nsamples*Nsamples - 1)/12;
    // Numerator: sum (t_i - t_avg)*(phase_i - phase_avg)
    double num = (dt*sumi - Nsamples*data_avg*t_avg);

    *slope = num / norm;
    *offset = data_avg - (*slope) * t_avg;
}

void extract_and_unwrap_phase(
    int i_start, size_t Nsamples,
    double* data_magnitude, double* data_phase,
    double* phases,
    double threshold_magnitude, double threshold_angle)
{
    double previous_phase = data_phase[i_start];
    double phase, diff, total_phase = 0;
    unsigned i;
    for(i = 0; i < Nsamples; ++i) {
    // The phase delivered by LLRF will wrap around at +-180 degrees. This will cause
    //  large jumps in the phase. If the phase from the cavity were constant,
    //  these jumps would always be 360 degrees.
    // However, the cavity signal does not necessarily remain constant neither
    //  in phase nor in amplitude and thus the jumps are sometimes smaller.
    // The following thus detects jumps above a(n expert) configurable
    //  threshold and also checks the signal magnitude to avoid acting
    //  on noise.
        phase = data_phase[i_start + i];
        diff = phase - previous_phase;
        if(fabs(diff) > threshold_angle*360 &&
            data_magnitude[i_start + i] > threshold_magnitude) {
            total_phase -= 360 * diff/fabs(diff);
        }
        previous_phase = phase;
        phases[i] = total_phase + phase;
    }
}


/**
 * @brief: Calculate de-tuning using decay method
 *
 *epicsUInt32 i_start,
        double threshold_magnitude, double threshold_angle,
        double * t_avg, double * phase_avg, double * detuning
 * @param [in]  i_start             index of start position to calculate
 * de-tuning
 * @param [in]  i_end               index of end position to calculate
 * de-tuning
 * @param [in]  dt                  time between samples
 * @param [in]  data_magnitude      array of magnitude values from cavity
 * @param [in]  data_phase          array of angle values from cavity
 * @param [in]  Nin                 number of elements on magnitude/phase
 * @param [in]  threshold_magnitude Threshold in magnitude of phase
 * wrap-around detection (expert setting)
 * @param [in]  threshold_angle     Threshold in phase of phase wrap-around
 * detection (expert setting)
 * @param [out] t_avg               Time mean
 * @param [out] phase_avg           Phase mean
 * @param [out] detuning            De-tuning value calculated using decay
 * method (Hz)
 *
 * Calculates the detuning of a cavity by fitting the
 *   change of the phase during the decay of the pulse.
 */
void calc_detuning_decay(int i_start,
        int i_end,double dt, double* data_magnitude,
        double* data_phase, size_t Nin,
        double threshold_magnitude, double threshold_angle,
        double* detuning) {

    size_t const Nsamples = (i_end - i_start);
    double phases[Nsamples];

    // Unwrap phase
    extract_and_unwrap_phase(i_start, Nsamples,
        data_magnitude, data_phase,
        phases,
        threshold_magnitude, threshold_angle);

    // Calculate detuning by linear regression
    double offset, slope;
    linear_regression(dt,
        phases, Nsamples,
        &offset, &slope);

    // Detuning: 1/360 * slope * 1e6 (conv. to Hz, since dt in us)
    *detuning = 1e6 * slope / 360;
}


/**
 * @brief: Calculate de-tuning using shift phase method
 *
 *epicsUInt32 i_start,
        double threshold_magnitude, double threshold_angle,
        double * t_avg, double * phase_avg, double * detuning
 * @param [in]  i_start             index of start position to calculate
 * de-tuning
 * @param [in]  i_end               index of end position to calculate
 * de-tuning
 * @param [in]  dt                  time between samples
 * @param [in]  cavity_magnitude    array of magnitude values from cavity
 * @param [in]  cavity_phase        array of angle values from cavity
 * @param [in]  forward_magnitude   array of magnitude values from cavity
 * @param [in]  forward_phase       array of angle values from cavity
 * @param [in]  Nin                 number of elements on magnitude/phase
 * @param [in]  threshold_magnitude Threshold in magnitude of phase
 * wrap-around detection (expert setting)
 * @param [in]  threshold_angle     Threshold in phase of phase wrap-around
 * detection (expert setting)
 * @param [in] phase_offset         Calibration value for phase offset
 * @param [in] Ql                   Loaded cavity Q factor
 * @param [in] frequency_norminal   Nominal system frequency
 * @param [out] detuning            De-tuning value calculated using decay
 * method (Hz)
 *
 * Calculates the detuning of a cavity by using
 *  tan(phi_cavity - phi_forward) = 2 Ql delta f/f
 * with delta f = delta f_forward - delta f_fwd
 */
void calc_detuning_phase_shift(
        int i_start, int i_end,
        double dt,
        double* cavity_magnitude,  double* cavity_phase,
        double* forward_magnitude, double* forward_phase,
        size_t Nin,
        double threshold_magnitude, double threshold_angle,
        double phase_offset, double Ql,
        double frequency_nominal,
        double* detuning)
{
    size_t const Nsamples = (i_end - i_start);

    // Unwrap phase of forward and cavity signals and
    //  extract the signal in the given window
    double cavity_phase_unwrapped[Nsamples];
    extract_and_unwrap_phase(i_start, Nsamples,
        cavity_magnitude, cavity_phase,
        cavity_phase_unwrapped,
        threshold_magnitude, threshold_angle);
    double forward_phase_unwrapped[Nsamples];
    extract_and_unwrap_phase(i_start, Nsamples,
        forward_magnitude, forward_phase,
        forward_phase_unwrapped,
        threshold_magnitude, threshold_angle);

    // Calculate phase difference between the cavity (pickup) and forward
    //  signal and average. Substract phase_offset to account for
    //  differences in cable length, ...
    double dphi_sum = 0;
    size_t i;
    for(i = 0; i < Nsamples; ++i) {
        dphi_sum += (cavity_phase_unwrapped[i] - forward_phase_unwrapped[i]);
    }
    double const phase_difference = dphi_sum/Nsamples - phase_offset;

    //TODO: debug information
//    std::cerr << "Phase shift:"                  << dphi_sum/Nsamples << std::endl;
//    std::cerr << "Phase shift with calibration:" << phase_difference << std::endl;

    // Calculate the detuning of the forward signal
    //  (!= 0 when using the frequency tracking mode)
    //TODO: this can be shared with DETPULSE
    double detuning_forward;
    calc_detuning_decay(i_start, i_end, dt,
        forward_magnitude, forward_phase, Nsamples,
        threshold_magnitude, threshold_angle,
        &detuning_forward);

    // Cavity detuning calculated from phase difference between
    //  cavity and forward signal, and detuning of the forward signal.
    double const detuning_from_forward = (frequency_nominal + detuning_forward)*tan(phase_difference)/(2*Ql);

    //TODO: debug information
//    std::cerr << "Detuning from forward:" << detuning_from_forward << std::endl;
//    std::cerr << "Forward detuning:"      << detuning_forward << std::endl;

    *detuning = detuning_forward + detuning_from_forward;
}



/*
 * Calculate detuning using decay method
 *
 * Parameters:
 *  INA: Phase waveform from LLRF
 *  INB: Magnitude waveform from LLRF
 *  INC: End of the pulse in INA/INB (in us)
 *  IND: Start of the fitting window after INC (in us)
 *  INE: Length of the fitting window (in us)
 *  INF: Sampling frequency from LLRF (LLRF:F-SAMPLING)
 *  ING: Near-IQ N from LLRF (LLRF:NEARIQN)
 *  INH: Threshold in phase of phase wrap-around detection (expert setting)
 *  INI: Threshold in magnitude of phase wrap-around detection (expert setting)
 *  INJ: Enable debug output on IOC shell (expert setting)
 *
 * Output:
 *  VALA: Detune value
 *  VALB: Quality factor
 */
static long detuning_calculate(aSubRecord *psr)
{

  short debug = (*(char*)psr->j != 0);

  /*
   * Input data
   */
  // Number of data points in data_phase and data_magnitude
  unsigned const Nin = psr->nea;
  // phase and magnitude array need to have the
  //  same number of points
  if(psr->neb != Nin)
    return INVALID_ARRAY_SIZE;

  double * data_phase     = (double*)psr->a;
  double * data_magnitude = (double*)psr->b;

  if(debug) printf("Points in: %d\n", Nin);

  /*
   * LLRF parameters & configuration
   */
  // LLRF Sampling frequency
  double const fsampling = *((double*)psr->f);
  // LLRF f0 (System Frequency)
  double const f0 = *((double*)psr->m);
  // Near-IQ N
  double const iqn = *((double*)psr->g);
  // Envelope data sampling rate
  double const sampling_rate = fsampling / iqn;
  // Time between samples
  double const dt = 1/sampling_rate;

  if(debug) printf("Sample spacing: %f us\n", dt);

  // Threshold for the phase wrap-around detection.
  //  Default to standard values if not set otherwise.
  double threshold_angle = *(double*)psr->h;
  double threshold_magnitude = *(double*)psr->i;

  /*
   * Time window for fitting
   */
  // End of the RF pulse
  double const t_endpulse = *((double*)psr->c);
  // Defines the length of the time window used for detuning calculation
  double const t_length   = *((double*)psr->e);
  // Start of the detuning calculation window from 0 in data arrays
  double       t_start    = t_endpulse + *((double*)psr->d);
  // Start of the detuning calculation window from 0 in data arrays
  double       t_end      = t_start + t_length;

  if(debug) printf("Time window requested: %f .. %f us\n", t_start, t_end);

  // Check for invalid parameters
  if(t_start < 0 || t_end < 0 || t_end < t_start)
    return RANGE_ERROR;

  // check if there is decimation
  short dec_en = *(short*)psr->k;
  long dec_fac = 1;
  if (dec_en != 0)
      dec_fac = *(long*)psr->l;

  if(debug) printf("Decimation enabled %d Decimation factor %ld us\n", dec_en,  dec_fac);
  // Calculate indices in data arrays from times
  unsigned const i_start = (unsigned)ceil(t_start / (dt * dec_fac));
  t_start = dt * i_start;
  unsigned const i_end   = (unsigned)ceil(t_end   / (dt * dec_fac));
  t_end = dt * (i_end - 1);

  if(debug) printf("Indices: %d .. %d, %d samples\n", i_start, i_end, i_end - i_start);
  if(debug) printf("Time window used: %f .. %f us\n", t_start, t_end);

  if(i_end <= i_start || i_start > Nin-1 || i_end > Nin-1) {
    if(debug) printf("Range error, Nin %d\n", Nin);
    return RANGE_ERROR;
  }

  // Check output has enough space
  int const Nsamples = (i_end - i_start);

   calc_detuning_decay(i_start, i_end, dt, data_magnitude,
        data_phase, Nin, threshold_magnitude, threshold_angle,((double*)psr->vala));

  // detuning value
  psr->neva = 1;

  //calculate quality factor
    double magnitude[Nsamples];

    /*
     * Calculate logarithm of ratio to first value
     */
    int i;
    for(i = 0; i < Nsamples; ++i) {
        magnitude[i] = log(data_magnitude[i + i_start]);
    }

    /*
     * Calculate detuning by linear regression
     */
    double offset, slope, Ql;
    linear_regression(dt,
        magnitude, Nsamples,
        &offset, &slope);

    // Detuning: 1e-6 * pi * f0 / slope (1e-6, since dt in us)
    Ql = -1e-6 * M_PI * f0*1e6 / slope;

   psr->nevb = 1;
   *((double*)psr->valb) = Ql;

  return 0;
}

/*
 * Calculate detuning using phase shift method
 *
 * Parameters:
 *  INA: Phase waveform from LLRF
 *  INB: Magnitude waveform from LLRF
 *  INC: End of the pulse in INA/INB (in us)
 *  IND: Start of the fitting window after INC (in us)
 *  INE: Length of the fitting window (in us)
 *  INF: Sampling frequency from LLRF (LLRF:F-SAMPLING)
 *  ING: Near-IQ N from LLRF (LLRF:NEARIQN)
 *  INH: Threshold in phase of phase wrap-around detection (expert setting)
 *  INI: Threshold in magnitude of phase wrap-around detection (expert setting)
 *  INJ: Enable debug output on IOC shell (expert setting)
 *
 * Output:
 *  VALA: Detune value
 *  VALB: Quality factor
 */
static long detuning_calculate_phase_shift(aSubRecord *psr)
{
  short debug = (*(char*)psr->t != 0);

  /*
   * Input data
   */
  // Number of data points in data_phase and data_magnitude
  unsigned const Nin = psr->nea;

  // check if there is decimation
  short dec_en_cav = *(short*)psr->p;
  long dec_fac_cav = 1;
  if (dec_en_cav != 0)
      dec_fac_cav = *(long*)psr->q;

  short dec_en_fwd = *(short*)psr->r;
  long dec_fac_fwd = 1;
  if (dec_en_fwd != 0)
      dec_fac_fwd = *(long*)psr->s;

  // phase and magnitude array need to have the
  //  same number of points and if there is
  //  decimation should be the same
  if(psr->neb != Nin || dec_fac_cav != dec_fac_fwd)
    return INVALID_ARRAY_SIZE;

  double * cavity_phase     = (double*)psr->a;
  double * cavity_magnitude = (double*)psr->b;
  double * forward_phase     = (double*)psr->c;
  double * forward_magnitude = (double*)psr->d;

  if(debug) printf("Points in: %d\n", Nin);

  /*
   * LLRF parameters & configuration
   */
  // LLRF Sampling frequency
  double const fsampling = *((double*)psr->h);
  // Near-IQ N
  double const iqn = *((double*)psr->i);
  // Envelope data sampling rate
  double const sampling_rate = fsampling / iqn;
  // Time between samples
  double const dt = 1/sampling_rate;

  if(debug) printf("Sample spacing: %f us\n", dt);

  // Threshold for the phase wrap-around detection.
  //  Default to standard values if not set otherwise.
  double threshold_angle = *(double*)psr->j;
  double threshold_magnitude = *(double*)psr->k;

  /*
   * Time window for fitting
   */
  // End of the RF pulse
  double const t_endpulse = *((double*)psr->e);
  // Defines the length of the time window used for detuning calculation
  double const t_length   = *((double*)psr->g);
  // Start of the detuning calculation window from 0 in data arrays
  double       t_start    = t_endpulse + *((double*)psr->f);
  // Start of the detuning calculation window from 0 in data arrays
  double       t_end      = t_start + t_length;

  if(debug) printf("Time window requested: %f .. %f us\n", t_start, t_end);

  // Check for invalid parameters
  if(t_start < 0 || t_end < 0 || t_end < t_start)
    return RANGE_ERROR;

  // Calculate indices in data arrays from times
  unsigned const i_start = (int)ceil(t_start / (dt * dec_fac_cav));
  t_start = dt * i_start;
  unsigned const i_end   = (int)ceil(t_end   / (dt * dec_fac_cav));
  t_end = dt * (i_end - 1);

  if(debug) printf("Indices: %d .. %d, %d samples\n", i_start, i_end, i_end - i_start);
  if(debug) printf("Time window used: %f .. %f us\n", t_start, t_end);

  if(i_end <= i_start || i_start > Nin-1 || i_end > Nin-1) {
    if(debug) printf("Range error, Nin %d\n", Nin);
    return RANGE_ERROR;
  }

  double Ql;
  if (*((short*)psr->n) == 0)
      Ql = *((double*)psr->l); // manual value
  else
      Ql = *((double*)psr->o); // from detune decay

  double phase_offset = *((double*)psr->m);
  double f0 = *((double*)psr->u);

    calc_detuning_phase_shift(i_start, i_end, dt,
        cavity_magnitude, cavity_phase,
        forward_magnitude, forward_phase,
        Nin, threshold_magnitude, threshold_angle,
        phase_offset, Ql,
        1e6*f0, (double*)psr->vala);

    // detuning value
    psr->neva = 1;

    return 0;
}

/*
 * Calculate the moving average
 *  INPA: Value to average over
 *  INPB: A value inhibiting the averaging
 *  INPC: Number of points Navg to average over
 *
 * Output:
 *  VALA: Moving average
 *  VALB: Last Navg points
 *  VALC: Actual number of points in array
 *    (needs to be tracked, since EPICS insists
 *     on initialising arrays with one element)
 */
static long detuning_moving_average(aSubRecord *psr)
{
  // Inhibit averaging if input b is not 0.
  double const interlock = *((char*)psr->b);
  if(interlock != 0)
    return 1;

  // Input value
  double const v = *((double*)psr->a);
  // Requested averaging points
  unsigned int const Nuser = *(int*)psr->c;
  // Only use user requested number of points, when it is positive
  //  and there's enough space in the EPICS array.
  int const Nmax = (Nuser > 0 && Nuser < psr->novb) ? Nuser : psr->novb;
  // Number of points presently
  int Nval = *(int*)psr->ovlc;
  //
  double threshold = *(double*)psr->d;

  // Previous array
  double* array_in  = (double*)psr->ovlb;
  // Output array
  double* array_out = (double*)psr->valb;
  // Time stamp array
  double* timestamp_in = (double*)psr->ovlc;
  // Time stamp array
  double* timestamp_out = (double*)psr->valc;

  // Get timestamp
  struct timespec stamp;
  clock_gettime(CLOCK_MONOTONIC, &stamp);
  double timestamp = stamp.tv_sec + 1e-9 * stamp.tv_nsec;

  int i, pos_overwrite;
  // Overwrite the last (empty) entry
  if(Nval < Nmax) {
    pos_overwrite = Nval;
    Nval++;
  // Find the right point to insert
  } else if (Nval == Nmax) {
    int pos_oldest = 0;
    double stamp_oldest = 1e99;
    for(i = 0; i < Nval; ++i) {
      if(timestamp_in[i] < stamp_oldest) {
        stamp_oldest = timestamp_in[i];
        pos_oldest = i;
      }
    }

    pos_overwrite = pos_oldest;
  // List too large: length was changed
  //  This only happens on the rate occasion where
  //  the user reduces the size.
  //  Just remove the last entries
  } else {
    Nval = Nmax;
    pos_overwrite = Nmax-1;
  }

  bool value_inserted = false;
  int d = 0;
  for(i = 0; i < Nval; ++i) {
    if(i == pos_overwrite && !value_inserted)
      d++;
    if(!value_inserted && (v < array_in[i+d] || i+d == Nval)) {
      array_out[i] = v;
      timestamp_out[i] = timestamp;
      value_inserted = true;
      d--;
    } else {
      array_out[i] = array_in[i+d];
      timestamp_out[i] = timestamp_in[i+d];
    }
  }

  double median = array_out[Nval / 2];

  // Loop over all valid points and
  //  - shift them forward by one, overwriting the last element
  //  - add them for calculating the average
  double sum = 0;
  int N = 0;
  for(i = 0; i < Nval; ++i) {
    if(abs(array_out[i] - median) < threshold) {
      sum += array_out[i];
      N++;
    }
  }

  // Output average, update array size of VALB and
  //  pass number of points for later use in VALC
  *((double*)psr->vala) = N > 1 ? sum/N : 0;
  psr->nevb             = Nval;
  *((int*)psr->valc)    = Nval;

  if(N == 0)
    return NO_VALID_SAMPLE;

  return 0;
}

// Register the functions for access by EPICS
#include <registryFunction.h>
#include <epicsExport.h>

epicsRegisterFunction(detuning_calculate);
epicsRegisterFunction(detuning_moving_average);
epicsRegisterFunction(detuning_calculate_phase_shift);
