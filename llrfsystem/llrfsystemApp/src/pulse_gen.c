#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <aSubRecord.h>

#include "pulse_gen.h"


/*
 * Include New element on calibration tables
 *
 * Parameters:
 * A: Enable pulse generation
 * C: Enable tracking
 * D: Delta_f
 * E: Phase
 * F: Table size
 * G: Sampling rate
 *
 * Output:
 * A: FF Angle waveform
 */
//
static long freq_tracking(aSubRecord *psr){
    short const gen_enabled      =  *((short*)  psr->a);
    long angle_cur_size          =  *((long*)   psr->b);
    short const tracking_enabled =  *((short*)  psr->c);
    double delta_f               =  *((double*) psr->d);
    double const phase           =  *((double*) psr->e);
    long const table_size        =  *((long*)   psr->f);
    double const fsampling       = (*((double*) psr->g))*1e6;
    double const IQN             =  *((double*) psr->h);


    //static feedback generation
    short  const bpresence_enabled   =  *((short*)  psr->i);
    long const bstart                =  *((long*) psr->j);
    long const bend                  =  *((long*) psr->k);
    double const angincr             =  *((double*) psr->l);
    short const statcompen           =  *((short*)  psr->m);


    double const sampling_rate = fsampling / IQN;

    // If generation not enable, don't touch table
    if(gen_enabled == 0) {
        psr->neva = angle_cur_size;
        return 0;
    }

    // If tracking not enabled, over-write detuning value
    if (tracking_enabled == 0) {
        delta_f = 0;
    }

    double * angles = (double*)psr->vala;
    long i;

    // if beam presence
    if (bpresence_enabled && statcompen)
        for (i = 0; i < table_size; i++) {
            angles[i] = 360*delta_f/sampling_rate*i + phase;
            angles[i] = 2*fmod(angles[i], 180) - fmod(angles[i], 360);
            if (i >= bstart && i <= bend)
                angles[i] += angincr;
        }
    else // if not beam presence (put the if before for to make it more efficient)
        for (i = 0; i < table_size; i++) {
            angles[i] = 360*delta_f/sampling_rate*i + phase;
            angles[i] = 2*fmod(angles[i], 180) - fmod(angles[i], 360);
        }

    psr->neva = table_size;

    return 0;
}


/*
 * Generate a pulse for FF Table
 *
 * Parameters:
 * A: Enable
 * B: Old table size
 * C: Sampling rate
 * D: Table size
 * E: Slope length
 * F: Pre-pulse length
 * G: Pre-pulse power ratio to pulse power
 * H: Pulse power
 *
 * Output:
 * A: FF Magnitude waveform
 */
//
static long pulse_gen(aSubRecord *psr){
    short  const gen_enabled   =  *((short*)  psr->a);
    long   const mag_cur_size  =  *((long*)   psr->b);
    double const fsampling     =  *((double*) psr->c);
    double const IQN           =  *((double*) psr->d);
    long   const table_size    =  *((long*)   psr->e);
    double const Tslope        = (*((double*) psr->f))*1e3;
    double const Tfill         = (*((double*) psr->g))*1e3;
    double const Pfill_ratio   =  *((double*) psr->h);
    double const P             =  *((double*) psr->i);

    //constant feedback generation
    short  const bpresence_enabled   =  *((short*)  psr->l);
    long const bstart              =  *((long*) psr->m);
    long const bend                =  *((long*) psr->n);
    double const magincr           =  *((double*) psr->o);
    short const statcompen         =  *((short*)  psr->p);

    // ramp down
    double const Tdown   =  *((double*) psr->q)*1e3;


    // If generation not enabled, don't touch table.
    if(gen_enabled == 0) {
        psr->neva = mag_cur_size;
        return 0;
    }

    // Error conditions
    if(table_size < 1 ||
       Tslope < 0 ||
       Tfill < 0 ||
       P < 0) {
      return 1;
    }

    double const sampling_rate = fsampling / IQN;

    double Pfill;
    // Only allow higher powers in pre-pulse than in main pulse
    if(Pfill_ratio < 1) {
        Pfill = P;
    } else {
        Pfill = P * Pfill_ratio;
    }

    // Calculate end point of each pulse segment and check for
    //  overrun with table size
    long i_end_slope = (long)(Tslope * sampling_rate);
    if(i_end_slope > table_size) i_end_slope = table_size;

    long i_end_fill  = (long)((Tfill + Tslope) * sampling_rate);
    if(i_end_fill > table_size) i_end_fill = table_size;

    //calculate the length of the ramp down
    long i_down = (long) (Tdown * sampling_rate);
    long start_down = 0;

    if (i_down < table_size)
        start_down = table_size - i_down;
    // if the ramp down is bigger than the table_size it should be table_size
    else
        start_down = table_size;

    // if there is a down ramp, it should be applied even over the rise and
    // fill time
    if (i_down > 0){
        if (i_end_slope > start_down)
            i_end_slope = start_down;

        if (i_end_fill > start_down)
            i_end_fill = start_down;
     }

    long i = 0;
    double * mag = (double*)psr->vala;

    if (bpresence_enabled && statcompen){ // use static compensation
        // Fill magnitude with slope up to Pfill in Tfill
        for (i = 0; i < i_end_slope; i++) {
            if (i >= bstart && i <= bend)
                mag[i] = ((Pfill / i_end_slope) * i) + magincr;
            else
                mag[i] = (Pfill / i_end_slope) * i;
        }
        // Fill magnitude with constant power Pfill in pre-pulse
        for (i = i_end_slope; i < i_end_fill; i++) {
            if (i >= bstart && i <= bend)
                mag[i] = Pfill + magincr;
            else
                mag[i] = Pfill;
        }
        // Fill magnitude with constant power P in pulse
        for (i = i_end_fill; i < start_down; i++) {
            if (i >= bstart && i <= bend)
                mag[i] = P + magincr;
            else
                mag[i] = P;
        }
    }
    else { // don't use static compensation
        // Fill magnitude with slope up to Pfill in Tfill
        for (i = 0; i < i_end_slope; i++) {
            mag[i] = (Pfill / i_end_slope) * i;
        }
        // Fill magnitude with constant power Pfill in pre-pulse
        for (i = i_end_slope; i < i_end_fill; i++) {
            mag[i] = Pfill;
        }
        // Fill magnitude with constant power P in pulse
        for (i = i_end_fill; i < start_down; i++) {
            mag[i] = P;
        }

    }

    //if there is a ramp down, apply it
    if (i_down > 0){
        int j = 0;
        for (i = start_down; i < table_size; i++){
            mag[i] = (P-((P/i_down)*j));
            j+=1;
        }
    } else {
        // Set the last point to P to avoid that the filling power
        //  is set along the full pulse should the table be too short
        mag[table_size - 1] = P;
    }



    psr->neva = table_size;

    return 0;
}


/*
 * Generate a ramping SP table
 *
 * Parameters:
 * A: Enable
 * B: Old table size
 * C: Sampling rate (MHz)
 * D: Near IQ N
 * E: Table size
 * F: Filling time (ms)
 * G: Tao (ms)
 * H: Pulse power
 * I: Phase value
 * J: Delay (ms)
 *
 * Output:
 * A: SP Magnitude waveform
 */
//
static long sp_ramping(aSubRecord *psr){
    short  const gen_enabled   =  *((short*)  psr->a);
    long   const mag_cur_size  =  *((long*)   psr->b);
    double const fsampling     =  *((double*) psr->c);
    double const IQN           =  *((double*) psr->d);
    long   const table_size    =  *((long*)   psr->e);
    double const Tfill         = (*((double*) psr->f))*1e3; // us
    double const tao           = (*((double*) psr->g))*1e3; // us
    double const P             =  *((double*) psr->h);
    double const phase_value   =  *((double*) psr->i);
    double const Tdelay        = (*((double*) psr->j))*1e3; // us
    double const Tdown        =  (*((double*) psr->k))*1e3; // us

    // If generation not enabled, don't touch table.
    if(gen_enabled == 0) {
        psr->neva = mag_cur_size;
        return 0;
    }

    // Error conditions
    if(table_size < 1 ||
       Tfill < 0 ||
       P < 0) {
      return 1;
    }

    double const sampling_rate = fsampling / IQN;

    // Calculate end point for filling time
    // overrun with table size
    long delay_samples = Tdelay * sampling_rate;
    long ramp_samples = Tfill * sampling_rate;
    long tao_samples = tao * sampling_rate;

    //calculate the length of the ramp down
    long i_down = (long) (Tdown * sampling_rate);
    long start_down = 0;

    if (i_down < table_size)
        start_down = table_size - i_down;
    // if the ramp down is bigger than the table_size it should be table_size
    else
        start_down = table_size;

    long i_end_fill = delay_samples + ramp_samples;
    if (i_end_fill > table_size) i_end_fill = table_size;

    // if there is a down ramp, it should be applied even over the fill time
    if (i_down > 0)
        if (i_end_fill > start_down)
            i_end_fill = start_down;

    long i = 0;
    double * mag = (double*)psr->vala;
    double * ang = (double*)psr->valb;
    double t;

    double fill_ratio = 1 / (1 - exp((double)(-ramp_samples)/tao_samples));

    // Fill magnitude with zeros for Tdelay
    for (i = 0; i < delay_samples; i++) {
        mag[i] = 0;
    }
    // Fill magnitude with slope up to P in Tfill
    for (i = delay_samples; i < i_end_fill; i++) {
        t = i - delay_samples;
        mag[i] = fill_ratio*P*(1 - exp(-t/tao_samples));
    }
    // Fill magnitude with constant power P in pulse
    for (i = i_end_fill; i < start_down; i++) {
        mag[i] = P;
    }

    //if there is a ramp down, apply it
    if (i_down > 0){
        int j = 0;
        for (i = start_down; i < table_size; i++){
            mag[i] = (P-((P/i_down)*j));
            j+=1;
        }
    } else {
        // Set the last point to P to avoid that the filling power
        //  is set along the full pulse should the table be too short
        mag[table_size - 1] = P;
    }

    // Fill phase table with the given constant value
    for (i = 0; i< table_size; i++) {
        ang[i] = phase_value;
    }

    psr->neva = table_size;
    psr->nevb = table_size;

    return 0;
}


// Register the functions for access by EPICS
#include <registryFunction.h>
#include <epicsExport.h>

epicsRegisterFunction(freq_tracking);
epicsRegisterFunction(pulse_gen);
epicsRegisterFunction(sp_ramping);
