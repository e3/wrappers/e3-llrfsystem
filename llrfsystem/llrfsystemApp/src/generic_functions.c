#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <aSubRecord.h>

#include "generic_functions.h"

// Reset a waveform
static long waveform_reset(aSubRecord *psr) {
    long const reset = *((long*)psr->a);
    long cur_size = *((long*)psr->b);

    if (reset == 0) {
        psr->neva = cur_size;
        return 0;
    } else {
        psr->neva = 0;
        return 0;
    }
}

// Copy a waveform
static long waveform_copy(aSubRecord *psr) {
    //Load input waveform
    double * in     = (double*)psr->a;

    long const in_len = *((long*)psr->b);

    // check if there is space on output waveform
    if (in_len > psr->nova) {
        return 1;
    }

    //copy waveform
    memcpy((double *) psr->vala, in, in_len * sizeof(double));

    //set size of the output
    psr->neva = in_len;

    return 0;
}


// Register the functions for access by EPICS
#include <registryFunction.h>
#include <epicsExport.h>

epicsRegisterFunction(waveform_reset);
epicsRegisterFunction(waveform_copy);
