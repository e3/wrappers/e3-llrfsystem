#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <aSubRecord.h>

#include "picalib.h"


/*
 * Include New element on calibration tables
 *
 * Parameters:
 * A: New EGU value
 * B: New Raw Value
 * C: EGU Len
 * D: Raw Len
 *
 * Output:
 * EGU Table
 * Raw Table
 */
static long calib_newelement(aSubRecord *psr)
{
    //Load current tables
    double * egu     = (double*)psr->vala;
    double * raw     = (double*)psr->valb;

    double const new_egu = *((double*)psr->a);
    double const new_raw = *((double*)psr->b);

    long const egu_len = *((long*)psr->c);
    long const raw_len = *((long*)psr->d);

    //check if there is space on array to insert the elements
    //and if the value is being inserted sorted
    if ((psr->nova > egu_len) && (psr->novb > raw_len))
        if ( (egu_len == 0 && raw_len == 0) || ( (new_egu >= egu[egu_len-1]) && (new_raw >= raw[raw_len-1]) ) ){
            egu[egu_len] = new_egu;
            raw[raw_len] = new_raw;
            psr->neva = egu_len+1;
            psr->nevb = raw_len+1;
            return 0;
        }
    // if it shouldn't insert, returns 1
    psr->neva = egu_len;
    psr->nevb = raw_len;
    return 1;
}

/*
 * Calculate statisc for new point. For a set of points calculate the
 * median then get x% points around the median and calculate the average.
 *
 * Parameters:
 * A: New value
 * B: Number of points used
 * C: Win size around median
 * D: Current size of the last inserted points
 *
 * Output:
 * A: Last inserted points
 * B: Statistical result
 */
static long calib_stat(aSubRecord *psr) {
    double const new_val = *((double*)psr->a);
    long const smp_nm = *((long*)psr->b);
    double const win_size = *((double*)psr->c);
    long cur_size = *((long*)psr->d);

    //last values
    double * vals    = (double*)psr->vala;

    //check if is enabled
    if (*((short*)psr->e) == 0) {
        psr->neva = cur_size;
        return 0;
    }

    //if number of points 0, do nothing
    if (smp_nm == 0) {
        psr->neva = 0;
        return 0;
    }

    int pos, avg_elem;
    double median, avg;

    if (cur_size < smp_nm) {
        if (cur_size == 0)
            pos = -1;
        else
            //move the elements to insert the new one on the right place
            for (pos = cur_size-1; pos >= 0 && vals[pos] > new_val; pos--)
                vals[pos+1] = vals[pos];
        vals[pos+1] = new_val;
        cur_size++;
    }
    else { // should shift the values
        for (pos = 0; pos < smp_nm-1 && vals[pos+1] < new_val ; pos++)
            vals[pos] = vals[pos+1];
        vals[pos] = new_val;
    }
    //calculate median
    if (cur_size % 2 != 0)
        median = vals[cur_size / 2];
    else
        median = (vals[cur_size / 2 - 1] + vals[cur_size / 2])/2;
    //select interval and calculate mean
    avg = 0;
    avg_elem = 0;
    int i = 0;
    for (i = 0; i < cur_size && vals[i] <= median*(1+win_size); i++){
        if (vals[i] >= median*(1-win_size)){
            avg += vals[i];
            avg_elem++;
        }
    }
    if (avg_elem != 0)
        avg /= avg_elem;
    else
        avg = 0;


   psr->neva = cur_size;

   psr->nevb = 1;
   *((double*)psr->valb) = avg;

   return 0;
}

// Register the functions for access by EPICS
#include <registryFunction.h>
#include <epicsExport.h>

epicsRegisterFunction(calib_newelement);
epicsRegisterFunction(calib_stat);
