##########
# Records to support FF automatic calibration
#### Macros:
# INPCH : Input channel to be used on automatic calibration

### General records for statistics (used by cavity and without cavity values)

record(bo, "$(P)$(R)FFCalStatsEn") {
    field(DESC, "Enable statistical analysis")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(VAL,  "0")
    field(PINI, "YES")

    info(DESCRIPTION, "Enable statistical analysis")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)FFCalStatsSmpNm") {
    field(DESC, "Set number of points for stats")

    info(DESCRIPTION, "Set number of points for stats")
    field(PREC, "0")
    field(VAL,  "10")
    field(PINI, "YES")
    field(DRVL, "1")
    field(DRVH, "$(TABLE_SMNM_MAX)")

    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)FFCalStatsWinSize") {
    field(DESC, "Win size for stats")
    field(PREC, "12")
    field(VAL,  "0.01")
    field(PINI, "YES")
    field(DRVL, "0")
    field(DRVH, "1")

    info(DESCRIPTION, "Win size for stats")
    info(ARCHIVE_THIS, "")
}

record(bo, "$(P)$(R)FFCalStatsRst") {
    field(DESC, "Reset Stats")

    info(DESCRIPTION, "Reset Stats")
    field(VAL,  "0")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(HIGH, "0.1")
    field(FLNK, "$(P)$(R)#FFCalStatsRst")
}

### Records to include new values on resultant calibration tables

record(aSub, "$(P)$(R)#FFCalNewVal") {
    field(DESC, "Incl. new element on FF calib ")

    info(DESCRIPTION, "Include new elements on FF calibration Tables")
    field(SNAM, "calib_newelement")
    field(INPA, "$(P)$(R)FFCalNewEGU")
    field(INPB, "$(P)$(R)FFCalNewRaw")
    field(INPC, "$(PD)$(RD)RFCtrlFF-CalEGU.NORD")  # Array sizes
    field(FTC,  "LONG")
    field(INPD, "$(PD)$(RD)RFCtrlFF-CalRaw.NORD")
    field(FTD,  "LONG")
    # EGU Array
    field(FTVA, "DOUBLE")
    field(NOVA, "$(TABLE_SMNM_MAX)")
    field(OUTA, "$(PD)$(RD)RFCtrlFF-CalEGU PP")
    # Raw Array
    field(FTVB, "DOUBLE")
    field(NOVB, "$(TABLE_SMNM_MAX)")
    field(OUTB, "$(PD)$(RD)RFCtrlFF-CalRaw PP")
}

record(ao, "$(P)$(R)FFCalNewEGU") {
    field(DESC, "Add new EGU for FF calibration ")

    info(DESCRIPTION, "Add new EGU for FF calibration")
    field(PREC, "12")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}

record(ao, "$(P)$(R)FFCalNewRaw") {
    field(DESC, "Add new Raw for FF calibration")

    info(DESCRIPTION, "Add new Raw for FF calibration")
    field(PREC, "12")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}

#### PVs for automatic calibration

record(bo, "$(P)$(R)FFAutoCalEn") {
    field(DESC, "Enable auto calibration")

    info(DESCRIPTION, "Enable auto calibration")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(VAL,  "0")

    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)FFAutoCalSmpNm") {
    field(DESC, "Set number of points for auto cal")

    info(DESCRIPTION, "Set number of points for auto cal")
    field(PREC, "0")
    field(VAL,  "5")
    field(PINI, "YES")
    field(DRVL, "1")

    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)FFAutoCalStr") {
    field(DESC, "Start val for auto cal")

    info(DESCRIPTION, "Start val for auto cal")
    field(PREC, "12")
    field(VAL,  "0")
    field(PINI, "YES")

    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)FFAutoCalEnd") {
    field(DESC, "End val for auto cal")

    info(DESCRIPTION, "End val for auto cal")
    field(PREC, "12")
    field(VAL,  "1")
    field(PINI, "YES")

    info(ARCHIVE_THIS, "")
}

# Support records to reset EGU and Raw waveforms

record(aSub, "$(P)$(R)#FFCalEGURst") {
    field(DESC, "Reset EGU waveform")

    info(DESCRIPTION, "Reset EGU waveform")
    field(SNAM, "waveform_reset")
    field(INPA, "$(P)$(R)FFCalEGURst")  # Drive the reset
    field(FTA,  "SHORT")
    field(INPB, "$(PD)$(RD)RFCtrlFF-CalEGU.NORD")  # Current size
    field(FTB,  "LONG")
    field(FTVA, "DOUBLE")
    field(NOVA, "$(TABLE_SMNM_MAX)")
    field(OUTA, "$(PD)$(RD)RFCtrlFF-CalEGU PP")
}

record(bo, "$(P)$(R)FFCalEGURst") {
    field(DESC, "Reset EGU")

    info(DESCRIPTION, "Reset EGU")
    field(VAL,  "0")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(HIGH, "0.1")
    field(FLNK, "$(P)$(R)#FFCalEGURst")
}

record(aSub, "$(P)$(R)#FFCalRawRst") {
    field(DESC, "Reset RAw waveform")

    info(DESCRIPTION, "Reset Raw waveform")
    field(SNAM, "waveform_reset")
    field(INPA, "$(P)$(R)FFCalRawRst")
    field(FTA,  "SHORT")
    field(INPB, "$(PD)$(RD)RFCtrlFF-CalRaw.NORD")  # Current size
    field(FTB,  "LONG")
    field(FTVA, "DOUBLE")
    field(NOVA, "$(TABLE_SMNM_MAX)")
    field(OUTA, "$(PD)$(RD)RFCtrlFF-CalRaw PP")
}

record(bo, "$(P)$(R)FFCalRawRst") {
    field(DESC, "Reset Raw")

    info(DESCRIPTION, "Reset Raw")
    field(VAL,  "0")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(HIGH, "0.1")
    field(FLNK, "$(P)$(R)#FFCalRawRst")
}

##### PVs to allow use magnitude and angle on fixed set pont
##### and fixed ramp at the same time. This is used to allow
##### automatic calibration with both modes (fixed and fixed ramp)

record(ao, "$(P)$(R)FFCal-Mag"){
    field(DESC, "Set mag on fixed and fixed ramp")
    field(PREC, "12")
    field(FLNK, "$(P)$(R)#FFCalSetMag")

    info(DESCRIPTION, "Set mag on fixed and fixed ramp")
    info(ARCHIVE_THIS, "")
}

record(seq, "$(P)$(R)#FFCalSetMag") {
    field(DESC, "Intern. PV to set mag")

    info(DESCRIPTION, "Intern. PV to set mag")
    field(DOL1, "$(P)$(R)FFCal-Mag")
    field(DOL2, "$(P)$(R)FFCal-Mag")
    field(LNK1, "$(PD)$(RD)RFCtrlCnstFF-Mag PP")
    field(LNK2, "$(P)$(R)FFPulseGenP PP")
}

record(ao, "$(P)$(R)FFCal-Ang"){
    field(DESC, "Set ang on fixed and fixed ramp")
    field(PREC, "4")
    field(DRVH, "180")
    field(DRVL, "-180")
    field(EGU,  "deg")
    field(FLNK, "$(P)$(R)#FFCalSetAng")

    info(DESCRIPTION, "Set ang on fixed and fixed ramp")
    info(ARCHIVE_THIS, "")
}

record(seq, "$(P)$(R)#FFCalSetAng") {
    field(DESC, "Intern. PV to set angle")

    info(DESCRIPTION, "Intern. PV to set angle")
    field(DOL1, "$(P)$(R)FFCal-Ang")
    field(DOL2, "$(P)$(R)FFCal-Ang")
    field(LNK1, "$(PD)$(RD)RFCtrlCnstFF-Ang PP")
    field(LNK2, "$(P)$(R)FFPulseGenPhase PP")
}

record(bo, "$(P)$(R)FFCalMode"){
    field(DESC, "Select mode")
    field(ZNAM, "Ramp")
    field(ONAM, "Constant")
    field(VAL,  "1")
    field(FLNK, "$(P)$(R)#FFCalModeInv")

    info(DESCRIPTION, "Select mode")
    info(ARCHIVE_THIS, "")
}

record(calc, "$(P)$(R)#FFCalModeInv"){
    field(INPA, "$(P)$(R)FFCalMode")
    field(CALC, "A=0?1:0")
    field(FLNK, "$(P)$(R)#FFCalMode")
}

record(seq, "$(P)$(R)#FFCalMode") {
    field(DESC, "Intern. PV to set/unset constant PI")

    info(DESCRIPTION, "Intern. PV to set/unset constant PI")
    field(DOL1, "$(P)$(R)FFCalMode")
    field(LNK1, "$(PD)$(RD)RFCtrlCnstFFEn PP")
    # Zero the power of PulseGenerator
    field(DOL2, "0")
    field(LNK2, "$(P)$(R)FFPulseGenP PP")
    # Enable/disable pulseGenerator
    field(DOL3, "$(P)$(R)#FFCalModeInv")
    field(LNK3, "$(P)$(R)FFPulseGenEn PP")
    # Set the phase
    field(DOL4, "1")
    field(LNK4, "$(P)$(R)#FFCalSetAng.PROC")
}
