## This database include records to automatically calculate the the static
# compensation values based on information from databuffer

record(bo, "$(P)$(R)$(PI_TYPE)AutoStatCompEn"){
    field(DESC, "Enable auto static compensation")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(VAL,  "0")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)AutoStatMag")

    info(DESCRIPTION, "Enable the automatic static compensation, based on a external PV")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)$(PI_TYPE)AutoStatCompK"){
    field(DESC, "Constant for auto stat. comp. mag.")
    field(VAL,  "0")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)AutoStatMag")
    field(PREC, "12")

    info(DESCRIPTION, "Constant K to calculate static compensation for magnitude")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)$(PI_TYPE)AutoStatCompX"){
    field(DESC, "Constant for auto stat. comp. ang.")
    field(VAL,  "0")
    field(PREC, "12")

    info(DESCRIPTION, "Constant X to calculate static compensation for angle")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(calcout, "$(P)$(R)#$(PI_TYPE)AutoStatMag"){
    field(DESC, "calc. auto. stat. mag")
    field(INPA, "$(P)$(R)$(PI_TYPE)AutoStatCompEn")
    field(INPB, "$(P)$(R)EVRBCurr")
    field(INPC, "$(P)$(R)EVRBeamNextPulse")
    field(INPD, "$(P)$(R)$(PI_TYPE)AutoStatCompK")
    field(INPE, "$(P)$(R)EVRIdCyc CP")
    field(CALC, "A=1&&C=1?B*D:0")
    field(OOPT, "On Change")
    field(OUT,  "$(P)$(R)$(PI_TYPE)StatCompMagInc PP")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)AutoStatAng")

    info(DESCRIPTION, "Calculate auto static compensation for magnitude")
}

record(calcout, "$(P)$(R)#$(PI_TYPE)AutoStatAng"){
    field(DESC, "calc. auto. stat. ang")
    field(INPA, "$(P)$(R)$(PI_TYPE)AutoStatCompEn")
    field(INPB, "$(P)$(R)EVRBCurr")
    field(INPC, "$(P)$(R)EVRBeamNextPulse")
    field(INPD, "$(P)$(R)$(PI_TYPE)AutoStatCompX")
    field(INPE, "$(P)$(R)EVRIdCyc CP")
    field(OOPT, "On Change")
    field(CALC, "A=1&&C=1?B*D:0")
    field(OUT,  "$(P)$(R)$(PI_TYPE)StatCompAngInc PP")

    info(DESCRIPTION, "Calculate auto static compensation for angle")
}
