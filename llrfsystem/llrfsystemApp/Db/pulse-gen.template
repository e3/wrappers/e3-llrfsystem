## Pulse generation records
# This database includes records for pulse generation (magnitude for FF),
# Frequency tracking (for phase),
# Static compensation (magnitude and phase)

# Pulse generation enable
record(bo, "$(P)$(R)$(PI_TYPE)PulseGenEn") {
    field(DESC, "Enable pulse generation")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(VAL,  "0")
    field(PINI, "YES")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)PulseGenChkPre")

    info(DESCRIPTION, "Enable pulse generation")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

# Pulse generation checks
record(calcout, "$(P)$(R)#$(PI_TYPE)PulseGenChkPre"){
    field(DESC, "Chk if enable to pre process")

    info(DESCRIPTION, "Check if Frequency tracking is enable to do the pre-processing")
    field(INPA, "$(P)$(R)$(PI_TYPE)PulseGenEn")
    field(CALC, "A=1?1:0")
    field(OOPT, "When Non-zero")
    field(OUT,  "$(P)$(R)#$(PI_TYPE)PulseGenPreProc.PROC")
}

record(seq, "$(P)$(R)#$(PI_TYPE)PulseGenPreProc") {
    field(DESC, "Ppre proc. pulse gen")

    info(DESCRIPTION, "Actions to execute after enabling pulse generation")
    # Table mode to "hold last"
    field(DOL0, "0")
    field(LNK0, "$(PD)$(RD)FFTbl-Mode PP")
    # New table value every pulse
    field(DOL1, "0")
    field(LNK1, "$(PD)$(RD)FFTbl-Spd PP")
    # Disable "Fixed SP/FF"
    field(DOL2, "0")
    field(LNK2, "$(PD)$(RD)RFCtrlCnst$(PI_TYPE)En PP")
    # Enable VM out
    field(DOL3, "1")
    field(LNK3, "$(PD)$(RR)RFOutEn PP")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)PulseGenProc")
}

# Total length of RF pulse
record(calc, "$(P)$(R)$(PI_TYPE)PulseGenT"){
    field(DESC, "Maximum pulse length")
    field(PREC, "3")
    field(EGU,  "ms")
    field(INPA, "$(PVRFWDT=$(PEVR)RFSyncWdt-SP) CP")
    field(CALC, "A>3800?3.8:A/1000")  # convert to ms. maxium 3.8 ms

    info(DESCRIPTION, "Maximum pulse length. Calculated from EVR RF Pulse Length")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)$(PI_TYPE)PulseGenTfill"){
    field(DESC, "Length of pre-pulse")
    field(PREC, "3")
    field(EGU,  "ms")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)PulseGenProc")

    info(DESCRIPTION, "Length of pre-pulse")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)$(PI_TYPE)PulseGenTslope"){
    field(DESC, "Length of power ramp")
    field(PREC, "3")
    field(EGU,  "ms")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)PulseGenProc")

    info(DESCRIPTION, "Length of power ramp")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)$(PI_TYPE)PulseGenTfall"){
    field(DESC, "Length of power ramp fall")
    field(PREC, "3")
    field(EGU,  "ms")
    field(VAL,  "0")
    field(DRVL, "0")
    field(DRVH, "3.5")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)PulseGenProc")

    info(DESCRIPTION, "Length of power ramp fall")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)$(PI_TYPE)PulseGenPfillRatio"){
    field(DESC, "Pre-pulse ratio to power in pulse")
    field(DRVH, "$(MAXFRAT=4.5)")
    field(DRVL, "$(MINFRAT=1)")
    field(PREC, "3")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)PulseGenProc")

    info(DESCRIPTION, "Pre-pulse ratio to power in pulse")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)$(PI_TYPE)PulseGenP"){
    field(DESC, "RF power in main pulse")
    field(PREC, "3")
    field(DRVH, "$(MAXPW=0)")
    field(DRVL, "$(MINPW=0)")
    field(EGU,  "kW")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)PulseGenProc")

    info(DESCRIPTION, "RF power in main pulse")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)$(PI_TYPE)PulseGenPhase"){
    field(DESC, "Pulse generation phase")
    field(PREC, "4")
    field(EGU,  "deg")
    field(DRVH, "180")
    field(DRVL, "-180")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)PulseGenProc")

    info(DESCRIPTION, "Pulse generation phase")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)FFTuningOffset") {
    field(DESC, "FF tuning offset")
    field(PREC, "4")
    field(EGU,  "deg")
    field(DRVH, "180")
    field(DRVL, "-180")
    field(FLNK, "$(P)$(R)#CalcFFPhase")

    info(DESCRIPTION, "FF Tuning Offset")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(calcout, "$(P)$(R)#CalcFFPhase") {
    field(DESC, "Calc FF Phase")
    field(CALC, "A&B=0?C+D:0")
    field(OOPT, "When Non-zero")
    field(INPA, "$(P)$(R)IsSCL")
    field(INPB, "$(PD)$(RD)OpenLoop-RB CP")
    field(INPC, "$(P)$(R)FFTuningOffset")
    field(INPD, "$(P)$(R)SPRampingPhase CP")
    field(OUT,  "$(P)$(R)FFPulseGenPhase")
    field(FLNK, "$(P)$(R)FFPulseGenPhase")
}

# Record for automatic behaviour for FF Phase
# disable when system is SCL and mode is not LLRF Expert and closed loop
# then only way of chaing will be via FFTuningOffset and SPRamping
record(calcout, "$(P)$(R)#FFPhaseAutoBhv") {
    field(DESC, "FF Phase auto bhv")
    field(CALC, "A&&B#0&&C=0?1:0")
    field(OOPT, "On Change")
    field(INPA, "$(P)$(R)IsSCL")
    field(INPB, "$(P)$(R)Mode-RB CP")
    field(INPC, "$(PD)$(RD)OpenLoop-RB CP")
    field(OUT,  "$(P)$(R)FFPulseGenPhase.DISP")
}

# Calculate size of table needed to cover whole RF pulse. Same
# table length is then used for amplitude and phase tables
record(calc, "$(P)$(R)#$(PI_TYPE)PulseGenTabS") {
    field(DESC, "Size of table")

    info(DESCRIPTION, "Size of table to cover the whole pulse")
    field(INPA, "$(P)$(R)$(PI_TYPE)PulseGenT CP")
    field(INPB, "$(P)$(R)FreqSampling")
    field(INPC, "$(PD)$(RD)IQSmpNearIQ-N-RB")
    field(CALC, "MAX(1,MIN(CEIL(1e3*A*B/C), 32768))")
    field(PINI, "YES")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)PulseGenProc")
}

record(calcout, "$(P)$(R)#$(PI_TYPE)PulseGenProc"){
    field(DESC, "Check if Pulse Gen should be processed")
    field(INPA, "$(P)$(R)$(PI_TYPE)PulseGenEn CP")
    field(CALC, "A=1?1:0")
    field(OOPT, "When Non-zero")
    field(OUT,  "$(P)$(R)#$(PI_TYPE)PulseGen.PROC")
}

record(aSub, "$(P)$(R)#$(PI_TYPE)PulseGen") {
    field(DESC, "RF power pulse generation")

    info(DESCRIPTION, "RF power pulse generation")
    field(SNAM, "pulse_gen")
    field(INPA, "$(P)$(R)$(PI_TYPE)PulseGenEn")
    field(FTA,  "SHORT")
    field(INPB, "$(PD)$(RD)$(PI_TYPE)Tbl-Mag.NORD")  # Size of magnitude array
    field(FTB,  "LONG")
    field(INPC, "$(P)$(R)FreqSampling")
    field(INPD, "$(PD)$(RD)IQSmpNearIQ-N-RB")
    field(INPE, "$(P)$(R)#$(PI_TYPE)PulseGenTabS")
    field(FTE,  "LONG")
    field(INPF, "$(P)$(R)$(PI_TYPE)PulseGenTslope")
    field(INPG, "$(P)$(R)$(PI_TYPE)PulseGenTfill")
    field(INPH, "$(P)$(R)$(PI_TYPE)PulseGenPfillRatio")
    field(INPI, "$(P)$(R)$(PI_TYPE)PulseGenP")
    # To trigger record:
    field(INPJ, "$(P)$(R)$(PI_TYPE)FreqTrackEn")
    field(INPK, "$(P)$(R)$(PI_TYPE)PulseGenPhase")
    # Static feed forward compensation
    field(INPL, "$(P)$(R)EVRBeamNextPulse")
    field(FTL,  "SHORT")
    field(INPM, "$(P)$(R)BStartPosSmp")
    field(FTM,  "SHORT")
    field(INPN, "$(P)$(R)EVRBEndPosSmp")
    field(FTN,  "SHORT")
    field(INPO, "$(P)$(R)$(PI_TYPE)StatCompMagInc")
    field(INPP, "$(P)$(R)$(PI_TYPE)StatCompEn")
    field(FTP,  "SHORT")
    # Ramp Fall
    field(INPQ, "$(P)$(R)$(PI_TYPE)PulseGenTfall")
    # Magnitude Array
    field(FTVA, "DOUBLE")
    #field(NOVA, "$(TABLE_SMNM_MAX)")
    field(NOVA, "32768")
    field(OUTA, "$(PD)$(RD)$(PI_TYPE)Tbl-Mag PP")
    field(DISA, "1")
    # Trigger generation of phase table after magnitude
    field(FLNK, "$(P)$(R)#$(PI_TYPE)FreqTrack")
}

# Frequency Tracking records
record(bo, "$(P)$(R)$(PI_TYPE)FreqTrackEn") {
    field(DESC, "Enable Frequency Tracking")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(VAL,  "0")
    field(PINI, "YES")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)FreqTrack")

    info(DESCRIPTION, "Enable Frequency Tracking")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

# Record for automatic behaviour for freq tracking
# set offset to 0 and disable:
# - when system in SCL/TS2 and mode is not LLRF Expert
# - when system in NCL and loop is closed
record(calcout, "$(P)$(R)#$(PI_TYPE)CalFreqTrackAutoBhv") {
    field(DESC, "Calc freq track auto bhv")
    field(CALC, "(A=1&&B#0)||(A=0&&C=0)?$(DIS=6):$(EN=9)")
    field(OOPT, "On Change")
    field(INPA, "$(P)$(R)IsSCL CP")
    field(INPB, "$(P)$(R)Mode-RB CP")
    field(INPC, "$(PD)$(RD)OpenLoop-RB CP")
    field(OUT,  "$(P)$(R)#$(PI_TYPE)FreqTrackAutoBhv.SELN")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)FreqTrackAutoBhv")
}

record(seq, "$(P)$(R)#$(PI_TYPE)FreqTrackAutoBhv") {
    field(DESC, "Freq tracking auto bhv")
    field(SELM, "Mask")
    field(DOL1, "0")
    field(LNK1, "$(P)$(R)$(PI_TYPE)FreqTrackOffset.DISP")
    field(DOL2, "0")
    field(LNK2, "$(P)$(R)$(PI_TYPE)FreqTrackOffset")
    field(DOL3, "1")
    field(LNK3, "$(P)$(R)$(PI_TYPE)FreqTrackOffset.DISP")
    field(DOL4, "1")
    field(LNK4, "$(P)$(R)$(PI_TYPE)FreqTrackOffset.PROC")
}

record(calcout, "$(P)$(R)#$(PI_TYPE)FreqTrackChkProc"){
    field(DESC, "Chk if enable to pre process")

    info(DESCRIPTION, "Check if Frequency tracking is enable to do the pre-processing")
    field(INPA, "$(P)$(R)$(PI_TYPE)FreqTrackEn")
    field(INPB, "$(P)$(R)$(PI_TYPE)PulseGenEn")
    field(CALC, "(A=1 && B=1)?1:0")
    field(OOPT, "When Non-zero")
    field(OUT,  "$(P)$(R)#$(PI_TYPE)FreqTrack.PROC")
    field(SCAN, "1 second")
}

record(aSub, "$(P)$(R)#$(PI_TYPE)FreqTrack") {
    field(DESC, "Frequency Tracking")

    info(DESCRIPTION, "Frequency Tracking")
    field(SNAM, "freq_tracking")
    field(INPA, "$(P)$(R)$(PI_TYPE)PulseGenEn")
    field(FTA,  "SHORT")
    field(INPB, "$(PD)$(RD)$(PI_TYPE)Tbl-Ang.NORD")  # Size of angles array
    field(FTB,  "LONG")
    field(INPC, "$(P)$(R)$(PI_TYPE)FreqTrackEn")
    field(FTC,  "SHORT")
    field(INPD, "$(P)$(R)$(PI_TYPE)FreqTrackDeltaF")
    field(INPE, "$(P)$(R)$(PI_TYPE)PulseGenPhase")
    field(INPF, "$(P)$(R)#$(PI_TYPE)PulseGenTabS")
    field(FTF,  "LONG")
    field(INPG, "$(P)$(R)FreqSampling")
    field(INPH, "$(PD)$(RD)IQSmpNearIQ-N-RB")
    # Static feed forward compensation
    field(INPI, "$(P)$(R)EVRBeamNextPulse")
    field(FTI,  "SHORT")
    field(INPJ, "$(P)$(R)BStartPosSmp")
    field(FTJ,  "SHORT")
    field(INPK, "$(P)$(R)EVRBEndPosSmp")
    field(FTK,  "LONG")
    field(INPL, "$(P)$(R)$(PI_TYPE)StatCompAngInc")
    field(INPM, "$(P)$(R)$(PI_TYPE)StatCompEn")
    field(FTM,  "SHORT")
    # Angle Array
    field(FTVA, "DOUBLE")
    #field(NOVA, "$(TABLE_SMNM_MAX)")
    field(NOVA, "32768")
    field(OUTA, "$(PD)$(RD)$(PI_TYPE)Tbl-Ang PP")
    field(DISA, "1")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)PulseGenChkPos")
}

record(mbbo, "$(P)$(R)$(PI_TYPE)FreqTrackSpd") {
    field(DESC, "Freq. Track. Speed update")
    field(OUT,  "$(P)$(R)#$(PI_TYPE)FreqTrackChkProc.SCAN")
    #    field(ZRVL, "9")
    #    field(ZRST, "0.1 second")
    #    field(ONVL, "8")
    #    field(ONST, "0.2 second")
    #    field(TWVL, "7")
    #    field(TWST, "0.5 second")
    #    field(THVL, "6")
    #    field(THST, "1 second")
    #    field(FRVL, "5")
    #    field(FRST, "2 second")
    #    field(FVVL, "4")
    #    field(FVST, "5 second")
    #    field(SXVL, "3")
    #    field(SXST, "10 second")
    #    field(VAL,  "3")
    field(NIVL, "9")
    field(NIST, "0.1 second")
    field(EIVL, "8")
    field(EIST, "0.2 second")
    field(SVVL, "7")
    field(SVST, "0.5 second")
    field(SXVL, "6")
    field(SXST, "1 second")
    field(FVVL, "5")
    field(FVST, "2 second")
    field(FRVL, "4")
    field(FRST, "5 second")
    field(THVL, "3")
    field(THST, "10 second")
    field(VAL,  "6")

    info(DESCRIPTION, "Time interval where the frequency tracking is updated")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(calc, "$(P)$(R)$(PI_TYPE)FreqTrackDeltaF"){
    field(DESC, "Freck Track Frequency")
    field(INPA, "$(P)$(R)#$(PI_TYPE)FreqTrackDeltaF CP")
    field(INPB, "$(P)$(R)$(PI_TYPE)FreqTrackOffset CP")
    field(CALC, "A+B")
    field(EGU,  "Hz")

    info(DESCRIPTION, "Frequency Tracking Frequency")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)#$(PI_TYPE)FreqTrackDeltaF"){
    field(DESC, "Freck Track Delta F")
    field(PREC, "12")

    info(DESCRIPTION, "Frequency Tracking Delta F")
}

record(ao, "$(P)$(R)$(PI_TYPE)FreqTrackOffset"){
    field(DESC, "Freck Track F offset")

    info(DESCRIPTION, "Frequency Tracking Offset from Delta F")
    field(EGU,  "Hz")
    field(PREC, "1")
    field(DRVH, "$(MAXFREQOFF=1000000.0)")
    field(DRVL, "$(MINFREQOFF=-1000000.0)")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(mbbo, "$(P)$(R)$(PI_TYPE)FreqTrackDetSrc"){
    field(DESC, "Freck Track Detune source")

    info(DESCRIPTION, "Frequency Tracking Detune source")
    field(OUT,  "$(P)$(R)#$(PI_TYPE)FreqTrackSelDet.SELN")
    field(ZRVL, "0")
    field(ZRST, "Off")
    field(ONVL, "1")
    field(ONST, "Decay")
    field(TWVL, "2")
    field(TWST, "Phase Shift")
    field(VAL,  "0")
    field(PINI, "YES")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(seq, "$(P)$(R)#$(PI_TYPE)FreqTrackSelDet") {
    field(DESC, "Intern. select. detune")

    info(DESCRIPTION, "Select detune source for Frequency Tracking")
    field(SELM, "Specified")
    field(DOL0, "0")
    field(DOL1, "$(P)$(R)DetuneDecayMovAvg CP")
    field(DOL2, "$(P)$(R)DetunePhaShftMovAvg CP")
    field(LNK0, "$(P)$(R)#$(PI_TYPE)FreqTrackDeltaF PP")
    field(LNK1, "$(P)$(R)#$(PI_TYPE)FreqTrackDeltaF PP")
    field(LNK2, "$(P)$(R)#$(PI_TYPE)FreqTrackDeltaF PP")
}

## After process Pulse generation
record(calcout, "$(P)$(R)#$(PI_TYPE)PulseGenChkPos"){
    field(DESC, "Chk if enable to post process")

    info(DESCRIPTION, "Check if pulse generation is enabled to do the post-processing")
    field(INPA, "$(P)$(R)$(PI_TYPE)PulseGenEn")
    field(CALC, " A=1?1:0")
    field(OOPT, "When Non-zero")
    field(OUT,  "$(P)$(R)#$(PI_TYPE)PulseGenPosProc.PROC")
}

# After process, call write table, set mode to circular and set magnitude ramp size
record(seq, "$(P)$(R)#$(PI_TYPE)PulseGenPosProc") {
    field(DESC, "Pos proc. pulse gen.")

    info(DESCRIPTION, "Actions post Pulse generation processing")
    field(DOL0, "1")
    field(LNK0, "$(PD)$(RD)$(PI_TYPE)Tbl-TblToFW PP")
}

###### Records for FF Static Compensation
#

record(bo, "$(P)$(R)$(PI_TYPE)StatCompEn"){
    field(DESC, "Enable static compensation")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(VAL,  "1")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)ChkStatCompProc")

    info(DESCRIPTION, "Static compensation for FeedForward table during the beam period")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)$(PI_TYPE)StatCompMagInc"){
    field(DESC, "Mag. Inc. for FF Stat. Comp.")
    field(VAL,  "0")
    field(EGU,  "kW")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)ChkStatCompProc")

    info(DESCRIPTION, "Static compensation for FeedForward table during the beam period")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)$(PI_TYPE)StatCompAngInc"){
    field(DESC, "Ang. Inc. for FF Stat. Comp.")
    field(VAL,  "0")
    field(PREC, "4")
    field(DRVH, "180")
    field(DRVL, "-180")
    field(EGU,  "deg")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)ChkStatCompProc")

    info(DESCRIPTION, "Angle increment Static compensation for FeedForward table during the beam period")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(calcout, "$(P)$(R)#$(PI_TYPE)ChkStatCompProc"){
    field(DESC, "Chk if should process pulse gen")
    field(INPA, "$(P)$(R)$(PI_TYPE)StatCompEn")
    field(CALC, " A=1?1:0")
    field(OOPT, "When Non-zero")
    field(OUT,  "$(P)$(R)#$(PI_TYPE)PulseGenProc.PROC")
}
