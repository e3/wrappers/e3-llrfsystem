#########
# Records for De-tuning using decay method
# Currently decay is calculated at each second
########
##### MACROS
# MAXSAMP: Maximum number of samples to be used on detuning
# MAXAVG: Maximum number of samples on moving average
# DWNMAXSIZE: Maximum number of elements on down-sampled channels
# PD, RD: Prefixes from the digitiser used on De-tuning
# CAVITYCH: Cavity channel number
# CAVFWDCH: Cavity forward number
# CH: Cavity identifier. Used when more than 1 detune is used

### Data from sis8300llrf
# Pulse done cnt
# Cavity channel magnitude and angle
# Cavity Forward magnitude and angle
# Near IQ N
# Interlock

####
# General configuration
####

record(bo, "$(P)$(R)Detune$(CH=)Run") {
    field(DESC, "Run de-tuning")

    info(DESCRIPTION, "Run de-tuning")
    field(ZNAM, "STOP")
    field(ONAM, "RUN")
    field(VAL,  "0")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(calcout, "$(P)$(R)#Detune$(CH=)CalcRun") {
    field(DESC, "Intern. control run/stop")

    info(DESCRIPTION, "Intern. control run/stop")
    field(INPA, "$(P)$(R)Detune$(CH=)Run")
    field(INPB, "$(PD)$(RD)PulseDoneCnt CP")
    field(CALC, "A=1?1:0")
    field(OOPT, "When Non-zero")
    field(OUT,  "$(P)$(R)#Detune$(CH=)Fanout.PROC")
}

record(fanout, "$(P)$(R)#Detune$(CH=)Fanout"){
    field(DESC, "Intern. fanout to process det")

    info(DESCRIPTION, "Intern. fanout to process det")
    field(LNK0, "$(P)$(R)#Detune$(CH=)DecayCalc")
    field(LNK1, "$(P)$(R)#Detune$(CH=)ForwardCalc")
    field(LNK2, "$(P)$(R)#Detune$(CH=)PhaShftCalc")
}

record(ao, "$(P)$(R)Detune$(CH=)WinPulseStart") {
    field(DESC, "Start of the first fitting win")

    info(DESCRIPTION, "Start of the first fitting win")
    field(EGU,  "us")
    field(VAL,  "0")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)Detune$(CH=)WinPulseLen") {
    field(DESC, "Length of the first fit. win.")

    info(DESCRIPTION, "Length of the first fit. win.")
    field(EGU,  "us")
    field(VAL,  "10")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)Detune$(CH=)WinDecayStart") {
    field(DESC, "Start of the fitting win after rfend")

    info(DESCRIPTION, "Start of the fitting win after rfend")
    field(EGU,  "us")
    field(VAL,  "0")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)Detune$(CH=)WinDecayLen") {
    field(DESC, "Length of the 2nd fit. win.")

    info(DESCRIPTION, "Length of the 2nd fit. win.")
    field(EGU,  "us")
    field(VAL,  "10")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

# Record to calculate RF pulse END
record(calc, "$(P)$(R)RFEnd$(CH=)") {
    field(DESC, "End of the RF pulse for de-tuning")
    field(INPA, "$(RFENDPV) CP")
    field(CALC, "A*1000")
    field(EGU,  "us")

    info(DESCRIPTION, "End of the RF pulse for de-tuning")
    info(ARCHIVE_THIS, "")
}

### Advanced parameters

record(ao, "$(P)$(R)Detune$(CH=)Thres-Mag") {
    field(DESC, "Threshold Magnitude for detuning")

    info(DESCRIPTION, "Threshold Magnitude for detuning")
    field(VAL,  "0.002")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)Detune$(CH=)Thres-Ang") {
    field(DESC, "Threshold Angle for detuning")

    info(DESCRIPTION, "Threshold Angle for detuning")
    field(VAL,  "0.5")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

#######
## PVs for Detuning using Method 1 - Phase Shift Method
#######

record(ao, "$(P)$(R)Detune$(CH=)PhaShftQL") {
    field(DESC, "In. qual. fac. for detuning pha shft")

    info(DESCRIPTION, "In. qual. fac. for detuning pha shft")
    field(VAL,  "0")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)Detune$(CH=)PhaShftPhaOff") {
    field(DESC, "In. phase offset for detuning pha shft")

    info(DESCRIPTION, "In. phase offset for detuning pha shft")
    field(VAL,  "0")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(bo, "$(P)$(R)Detune$(CH=)PhaShftSelQL"){
    field(DESC, "Select QL for detuning pha. shft.")

    info(DESCRIPTION,"Select QL for detuning pha. shft.")
    field(ZNAM, "MANUAL")
    field(ONAM, "FROM DECAY")
    field(VAL,  "0")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(aSub, "$(P)$(R)#Detune$(CH=)PhaShftCalc") {
    field(DESC, "Intern. calc. detune")

    info(DESCRIPTION, "Intern. calc. detune")
    field(SNAM, "detuning_calculate_phase_shift")
    field(INPA, "$(PD)$(RD)Dwn$(CAVITYCH=0)-Cmp1")  # Cavity Angle
    field(NOA,  "$(DWNMAXSIZE=40000)")
    field(INPB, "$(PD)$(RD)Dwn$(CAVITYCH=0)-Cmp0")  # Cavity Magnitude
    field(NOB,  "$(DWNMAXSIZE=40000)")
    field(INPC, "$(PD)$(RD)Dwn$(CAVFWDCH=2)-Cmp1")  # Cavity Forward Angle
    field(NOC,  "$(DWNMAXSIZE=40000)")
    field(INPD, "$(PD)$(RD)Dwn$(CAVFWDCH=2)-Cmp0")  # Cavity Forward Magnitude
    field(NOD,  "$(DWNMAXSIZE=40000)")
    field(INPE, "0")  # us
    field(INPF, "$(P)$(R)Detune$(CH=)WinPulseStart")  # us
    field(INPG, "$(P)$(R)Detune$(CH=)WinPulseLen")  # us
    field(INPH, "$(P)$(R)FreqSampling")  # TODO how to manage on multi-board system?
    field(INPI, "$(PD)$(RD)IQSmpNearIQ-N-RB")  # TODO how to manage on multi-board system?
    field(INPJ, "$(P)$(R)Detune$(CH=)Thres-Ang")
    field(INPK, "$(P)$(R)Detune$(CH=)Thres-Mag")
    field(INPL, "$(P)$(R)Detune$(CH=)PhaShftQL")  # quality factor (input)
    field(INPM, "$(P)$(R)Detune$(CH=)PhaShftPhaOff")  # phase offset (input)
    field(INPN, "$(P)$(R)Detune$(CH=)PhaShftSelQL")  # QL selector
    field(INPO, "$(P)$(R)Detune$(CH=)DecayQL")
    field(FTP,  "SHORT")
    field(INPP, "$(PD)$(RD)Dwn$(CAVITYCH=0)-DecEn-RB")  # decimation enable from cav ch
    field(FTQ,  "LONG")
    field(INPQ, "$(PD)$(RD)Dwn$(CAVITYCH=0)-DecF-RB")  # decimation factor from cav ch
    field(FTR,  "SHORT")
    field(INPR, "$(PD)$(RD)Dwn$(CAVFWDCH=2)-DecEn-RB")  # decimation enable from fwd ch
    field(FTS,  "LONG")
    field(INPS, "$(PD)$(RD)Dwn$(CAVFWDCH=2)-DecF-RB")  # decimation factor from fwd ch
    field(FTT,  "CHAR")  # DEBUG
    field(INPU, "$(P)$(R)FreqSystem")  # system frequency
    # detuning value
    field(FTVA, "DOUBLE")
    field(NOVA, "1")
    field(OUTA, "$(P)$(R)Detune$(CH=)PhaShft PP")
}

record(ai, "$(P)$(R)Detune$(CH=)PhaShft") {
    field(DESC, "Cav. de-tuning using pha shft method")

    info(DESCRIPTION, "Cav. de-tuning using pha shft method")
    field(DISP, "1")
    field(EGU,  "Hz")
    field(FLNK, "$(P)$(R)#Detune$(CH=)PhaShftAvg")

    info(ARCHIVE_THIS, "")
}

# PVs to monitor detuning return and set a message error
record(calcout, "$(P)$(R)#Detune$(CH=)PhaShftCStat"){
    field(INPA, "$(P)$(R)#Detune$(CH=)PhaShftCalc CPP")
    field(CALC, "-A")
    field(OUT,  "$(P)$(R)Detune$(CH=)PhashftStat PP")
}

record(mbbi, "$(P)$(R)Detune$(CH=)PhashftStat"){
    field(ONST, "INVALID ARRAYS SIZE")
    field(TWST, "RANGE ERROR")

    info(ARCHIVE_THIS, "")
}

#######
## PVs for Detuning using Method 2 - Decay Method
#######

record(aSub, "$(P)$(R)#Detune$(CH=)DecayCalc") {
    field(DESC, "Intern. calc. detune and ql")

    info(DESCRIPTION, "Intern. calc. detune and ql")
    field(SNAM, "detuning_calculate")
    field(BRSV, "MINOR")
    field(INPA, "$(PD)$(RD)Dwn$(CAVITYCH=0)-Cmp1")  # Cavity Angle
    field(NOA,  "$(DWNMAXSIZE=40000)")
    field(INPB, "$(PD)$(RD)Dwn$(CAVITYCH=0)-Cmp0")  # Cavity Magnitude
    field(NOB,  "$(DWNMAXSIZE=40000)")
    field(INPC, "$(P)$(R)RFEnd$(CH=)")  # us
    field(INPD, "$(P)$(R)Detune$(CH=)WinDecayStart")  # us
    field(INPE, "$(P)$(R)Detune$(CH=)WinDecayLen")  # us
    field(INPF, "$(P)$(R)FreqSampling")  # TODO how to manage on multi-board system?
    field(INPG, "$(PD)$(RD)IQSmpNearIQ-N-RB")  # TODO how to manage on multi-board system?
    field(INPH, "$(P)$(R)Detune$(CH=)Thres-Ang")
    field(INPI, "$(P)$(R)Detune$(CH=)Thres-Mag")
    field(FTJ,  "CHAR")  # DEBUG
    field(FTK,  "SHORT")
    field(INPK, "$(PD)$(RD)Dwn$(CAVITYCH=0)-DecEn-RB")  # decimation enable from cav ch
    field(FTL,  "LONG")
    field(INPL, "$(PD)$(RD)Dwn$(CAVITYCH=0)-DecF-RB")  # decimation factor from cav ch
    field(INPM, "$(P)$(R)FreqSystem")  # system frequency
    # detuning value
    field(FTVA, "DOUBLE")
    field(NOVA, "1")
    field(OUTA, "$(P)$(R)Detune$(CH=)Decay PP")
    # Quality factor
    field(FTVB, "DOUBLE")
    field(NOVB, "1")
    field(OUTB, "$(P)$(R)Detune$(CH=)DecayQL PP")
}

record(ai, "$(P)$(R)Detune$(CH=)Decay") {
    field(DESC, "Cavity de-tuning using decay method")

    info(DESCRIPTION, "Cavity de-tuning using decay method")
    field(DISP, "1")
    field(EGU,  "Hz")
    field(FLNK, "$(P)$(R)#Detune$(CH=)DecayAvg")

    info(ARCHIVE_THIS, "")
}

record(ai, "$(P)$(R)Detune$(CH=)DecayQL") {
    field(DESC, "Loaded quality from decay method")

    info(DESCRIPTION, "Loaded quality from decay method")
    info(ARCHIVE_THIS, "")
}

# PVs to monitor detuning return and set a message error
record(calcout, "$(P)$(R)#Detune$(CH=)DecayChkStat"){
    field(INPA, "$(P)$(R)#Detune$(CH=)DecayCalc CPP")
    field(CALC, "-A")
    field(OUT,  "$(P)$(R)Detune$(CH=)DecayStat PP")
}

record(mbbi, "$(P)$(R)Detune$(CH=)DecayStat"){
    field(ONST, "INVALID ARRAY SIZE")
    field(TWST, "RANGE ERROR")

    info(ARCHIVE_THIS, "")
}

######
# PVs to Calculate detune of the forward signal from reference
######

record(aSub, "$(P)$(R)#Detune$(CH=)ForwardCalc") {
    field(DESC, "Intern. calc. detune pulse")

    info(DESCRIPTION, "Intern. calc. detune pulse")
    field(SNAM, "detuning_calculate")
    field(INPA, "$(PD)$(RD)Dwn$(CAVITYCH=0)-Cmp1")  # Angle
    field(NOA,  "$(DWNMAXSIZE=40000)")
    field(INPB, "$(PD)$(RD)Dwn$(CAVITYCH=0)-Cmp0")  # Magnitude
    field(NOB,  "$(DWNMAXSIZE=40000)")
    field(INPC, "0")  # us
    field(INPD, "$(P)$(R)Detune$(CH=)WinPulseStart")  # us
    field(INPE, "$(P)$(R)Detune$(CH=)WinPulseLen")  # us
    field(INPF, "$(P)$(R)FreqSampling")  # TODO how to manage on multi-board system?
    field(INPG, "$(PD)$(RD)IQSmpNearIQ-N-RB")  # TODO how to manage on multi-board system?
    field(INPH, "$(P)$(R)Detune$(CH=)Thres-Ang")
    field(INPI, "$(P)$(R)Detune$(CH=)Thres-Mag")
    field(FTJ,  "CHAR")  # DEBUG
    field(FTK,  "SHORT")
    field(INPK, "$(PD)$(RD)Dwn$(CAVITYCH=0)-DecEn-RB")  # decimation enable from cav ch
    field(FTL,  "LONG")
    field(INPL, "$(PD)$(RD)Dwn$(CAVITYCH=0)-DecF-RB")  # decimation factor from cav ch
    field(INPM, "$(P)$(R)FreqSystem")  # system frequency
    # detuning value
    field(FTVA, "DOUBLE")
    field(NOVA, "1")
    field(OUTA, "$(P)$(R)Detune$(CH=)Forward PP")
}

record(ai, "$(P)$(R)Detune$(CH=)Forward") {
    field(DESC, "Cavity de-tuning using decay method")
    field(DISP, "1")
    field(EGU,  "Hz")

    info(DESCRIPTION, "Cavity de-tuning using decay method")
    info(ARCHIVE_THIS, "")
}

# PVs to monitor detuning return and set a message error
record(calcout, "$(P)$(R)#Detune$(CH=)ForwardChkStat"){
    field(INPA, "$(P)$(R)#Detune$(CH=)ForwardCalc CPP")
    field(CALC, "-A")
    field(OUT,  "$(P)$(R)Detune$(CH=)ForwardStat PP")
}

record(mbbi, "$(P)$(R)Detune$(CH=)ForwardStat"){
    field(ONST, "INVALID ARRAY SIZE")
    field(TWST, "RANGE ERROR")

    info(ARCHIVE_THIS, "")
}

######
# PVs for Calculate moving average for detune using decay method
######

record(ao, "$(P)$(R)Detune$(CH=)DecayAvgOvr") {
    field(DESC, "Number of positions on moving avg")

    info(DESCRIPTION, "Number of positions on moving avg")
    field(VAL,  "10")
    field(PINI, "YES")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)Detune$(CH=)DecayMovAvgThres") {
    field(DESC, "Moving average: rejection threshold")

    info(DESCRIPTION, "Moving average: rejection threshold around median")
    field(EGU,  "Hz")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

# Detuning calculation average
record(aSub, "$(P)$(R)#Detune$(CH=)DecayAvg") {
    field(DESC, "Intern. calc cavity detuning average")

    info(DESCRIPTION, "Intern. calc cavity detuning average")
    field(SNAM, "detuning_moving_average")
    field(INPA, "$(P)$(R)#Detune$(CH=)DecayCalc.VALA")
    field(INPB, "$(PD)$(RD)TrigILock")
    field(FTB,  "CHAR")
    field(INPC, "$(P)$(R)Detune$(CH=)DecayAvgOvr")
    field(FTC,  "LONG")
    field(INPD, "$(P)$(R)Detune$(CH=)DecayMovAvgThres")
    field(FTD,  "DOUBLE")
    field(OUTA, "$(P)$(R)Detune$(CH=)DecayMovAvg PP")  # moving average
    field(OUTB, "$(P)$(R)Detune$(CH=)DecayLstAvg PP")  # Last average
    field(OUTC, "$(P)$(R)Detune$(CH=)DecayLstStamps PP")
    field(FTVC, "DOUBLE")
    # Maximum averaging samples. Corresponds to 2.5 minutes at 14 Hz.
    field(NOVB, "$(MAXAVG=2048)")
    field(NOVC, "$(MAXAVG=2048)")
}

record(ai, "$(P)$(R)Detune$(CH=)DecayMovAvg") {
    field(DESC, "Moving average for detuning")
    field(DISP, "1")
    field(EGU,  "Hz")

    info(DESCRIPTION, "Moving average for detuning")
    info(ARCHIVE_THIS, "")
}

record(waveform, "$(P)$(R)Detune$(CH=)DecayLstAvg") {
    field(DESC, "Last moving average for detunin")

    info(DESCRIPTION, "Last moving average for detunin")
    field(FTVL, "DOUBLE")
    field(NELM, "$(MAXAVG=2048)")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}

record(waveform, "$(P)$(R)Detune$(CH=)DecayLstStamps") {
    field(DESC, "Time stamps for mov avg")

    info(DESCRIPTION, "Time stamps for mov avg")
    field(FTVL, "DOUBLE")
    field(NELM, "$(MAXAVG=2048)")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}

# PVs to monitor detuning return and set a message error
record(calcout, "$(P)$(R)#Detune$(CH=)DecayAvgCStat"){
    field(INPA, "$(P)$(R)#Detune$(CH=)DecayAvg CPP")
    field(CALC, "-A")
    field(OUT,  "$(P)$(R)Detune$(CH=)DecayAvgStat PP")
}

record(mbbi, "$(P)$(R)Detune$(CH=)DecayAvgStat"){
    field(ONST, "NO VALID SAMPLE")
}

######
# PVs for Calculate moving average for detune using phase shift
######
record(ao, "$(P)$(R)Detune$(CH=)PhaShftAvgOvr") {
    field(DESC, "Number of positions on moving avg")

    info(DESCRIPTION, "Number of positions on moving avg")
    field(VAL,  "10")
    field(PINI, "YES")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)Detune$(CH=)PhaShftMovAvgThres") {
    field(DESC, "Moving average: rejection window")

    info(DESCRIPTION, "Moving average: rejection threshold around median")
    field(EGU,  "Hz")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

# Detuning calculation average
record(aSub, "$(P)$(R)#Detune$(CH=)PhaShftAvg") {
    field(DESC, "Intern. calc detuning avg")

    info(DESCRIPTION, "Intern. calc detuning avg")
    field(SNAM, "detuning_moving_average")
    field(INPA, "$(P)$(R)#Detune$(CH=)PhaShftCalc.VALA")
    field(INPB, "$(PD)$(RD)TrigILock")
    field(FTB,  "CHAR")
    field(INPC, "$(P)$(R)Detune$(CH=)PhaShftAvgOvr")
    field(FTC,  "LONG")
    field(INPD, "$(P)$(R)Detune$(CH=)PhaShftMovAvgThres")
    field(FTD,  "DOUBLE")
    field(OUTA, "$(P)$(R)Detune$(CH=)PhaShftMovAvg PP")  # moving average
    field(OUTB, "$(P)$(R)Detune$(CH=)PhaShftLstAvg PP")  # Last average
    field(OUTC, "$(P)$(R)Detune$(CH=)PhaShftLstStamps PP")
    field(FTVC, "DOUBLE")
    # Maximum averaging samples. Corresponds to 2.5 minutes at 14 Hz.
    field(NOVB, "$(MAXAVG=2048)")
    field(NOVC, "$(MAXAVG=2048)")
}

record(ai, "$(P)$(R)Detune$(CH=)PhaShftMovAvg") {
    field(DESC, "Moving average for detuning")
    field(DISP, "1")
    field(EGU,  "Hz")

    info(DESCRIPTION, "Moving average for detuning")
    info(ARCHIVE_THIS, "")
}

record(waveform, "$(P)$(R)Detune$(CH=)PhaShftLstAvg") {
    field(DESC, "Last moving average for detunin")

    info(DESCRIPTION, "Last moving average for detunin")
    field(FTVL, "DOUBLE")
    field(NELM, "$(MAXAVG=2048)")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}

record(waveform, "$(P)$(R)Detune$(CH=)PhaShftLstStamps") {
    field(DESC, "Time stamps for mov avg")

    info(DESCRIPTION, "Time stamps for mov avg")
    field(FTVL, "DOUBLE")
    field(NELM, "$(MAXAVG=2048)")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
}

# PVs to monitor detuning return and set a message error
record(calcout, "$(P)$(R)#Detune$(CH=)PhaShftAvgCStat"){
    field(INPA, "$(P)$(R)#Detune$(CH=)PhaShftAvg CPP")
    field(CALC, "-A")
    field(OUT,  "$(P)$(R)Detune$(CH=)PhaShftAvgStat PP")
}

record(mbbi, "$(P)$(R)Detune$(CH=)PhaShftAvgStat"){
    field(ONST, "NO VALID SAMPLE")
}
