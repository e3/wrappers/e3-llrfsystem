####### PVs to set a ramp on PI Mag/Ang tables (start, end and nelm )
## MACROS
# PD, RD: Digitiser prefixes
#######

record(ao, "$(P)$(R)$(PI_TYPE)TblRampSmpNm") {
    field(DESC, "nr of elem. for ramp on mag/ang table")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)TblRampSmpNmS")
    field(PREC, "0")
    field(VAL,  "10")

    info(DESCRIPTION, "nr of elem. for ramp on mag/ang table")
    info(ARCHIVE_THIS, "")
}

record(seq, "$(P)$(R)#$(PI_TYPE)TblRampSmpNmS") {
    field(DESC, "Set nelm for mag/ang table")

    info(DESCRIPTION, "Set nelm for mag/ang table")
    field(DOL1, "$(P)$(R)$(PI_TYPE)TblRampSmpNm")
    field(DOL2, "$(P)$(R)$(PI_TYPE)TblRampSmpNm")
    field(LNK1, "$(P)$(R)$(PI_TYPE)TblRampSmpNm-Mag PP")
    field(LNK2, "$(P)$(R)$(PI_TYPE)TblRampSmpNm-Ang PP")
}

record(ao, "$(P)$(R)$(PI_TYPE)TblRampEnd-Mag") {
    field(DESC, "End value for ramp on mag table")

    info(DESCRIPTION, "End value for ramp on mag table")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)TblRamp-Mag")
    field(PREC, "12")
}

record(ao, "$(P)$(R)$(PI_TYPE)TblRampStr-Mag") {
    field(DESC, "Start value for ramp on mag table")

    info(DESCRIPTION, "Start value for ramp on mag table")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)TblRamp-Mag")
    field(PREC, "12")
    field(VAL,  "0")
}

record(ao, "$(P)$(R)$(PI_TYPE)TblRampSmpNm-Mag") {
    field(DESC, "nr of elem. for ramp on mag table")

    info(DESCRIPTION, "nr of elem. for ramp on mag table")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)TblRamp-Mag")
    field(PREC, "0")
    field(VAL,  "10")
    field(OUT,  "$(P)$(R)#$(PI_TYPE)TblRamp-Mag.NUSE")
}

record(acalcout, "$(P)$(R)#$(PI_TYPE)TblRamp-Mag") {
    field(DESC, "Create a simple ramp for magnitude")

    info(DESCRIPTION, "Create a simple ramp for magnitude")
    field(INPA, "$(P)$(R)$(PI_TYPE)TblRampStr-Mag")
    field(INPB, "$(P)$(R)$(PI_TYPE)TblRampEnd-Mag")
    field(INPC, "$(P)$(R)$(PI_TYPE)TblRampSmpNm-Mag")
    field(NELM, "$(PITABLE_SMNM_MAX)")
    field(NUSE, "10")
    field(SIZE, "NUSE")
    field(CALC, "A + IX*((B-A)/(C-1))")
    field(OUT,  "$(PD)$(RD)$(PI_TYPE)Tbl-Mag PP")
}

# For -Ang

record(ao, "$(P)$(R)$(PI_TYPE)TblRampEnd-Ang") {
    field(DESC, "End value for ramp on ang table")

    info(DESCRIPTION, "End value for ramp on ang table")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)TblRamp-Ang")
    field(PREC, "12")
}

record(ao, "$(P)$(R)$(PI_TYPE)TblRampStr-Ang") {
    field(DESC, "Start value for ramp on mag table")

    info(DESCRIPTION, "Start value for ramp on mag table")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)TblRamp-Ang")
    field(PREC, "12")
    field(VAL,  "0")
}

record(ao, "$(P)$(R)$(PI_TYPE)TblRampSmpNm-Ang") {
    field(DESC, "nr of elem. for ramp on mag table")

    info(DESCRIPTION, "nr of elem. for ramp on mag table")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)TblRamp-Ang")
    field(PREC, "0")
    field(VAL,  "10")
    field(OUT,  "$(P)$(R)#$(PI_TYPE)TblRamp-Ang.NUSE")
}

record(acalcout, "$(P)$(R)#$(PI_TYPE)TblRamp-Ang") {
    field(DESC, "Create a simple ramp for magnitude")

    info(DESCRIPTION, "Create a simple ramp for magnitude")
    field(INPA, "$(P)$(R)$(PI_TYPE)TblRampStr-Ang")
    field(INPB, "$(P)$(R)$(PI_TYPE)TblRampEnd-Ang")
    field(INPC, "$(P)$(R)$(PI_TYPE)TblRampSmpNm-Ang")
    field(NELM, "$(PITABLE_SMNM_MAX)")
    field(NUSE, "10")
    field(SIZE, "NUSE")
    field(CALC, "A + IX*((B-A)/(C-1))")
    field(OUT,  "$(PD)$(RD)$(PI_TYPE)Tbl-Ang PP")
}

#### PVs to set fixed value on table
# These PVs use the ramp PVs to "build" the fixed value on table

record(ao, "$(P)$(R)$(PI_TYPE)TblFix-Mag") {
    field(DESC, "Value for fixed magnitude table")

    info(DESCRIPTION, "Value for fixed magnitude table")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)TblFixS-Mag")
    field(PREC, "12")
}

record(seq, "$(P)$(R)#$(PI_TYPE)TblFixS-Mag") {
    field(DOL0, "$(P)$(R)$(PI_TYPE)TblFix-Mag")
    field(DOL1, "$(P)$(R)$(PI_TYPE)TblFix-Mag")
    field(LNK0, "$(P)$(R)$(PI_TYPE)TblRampStr-Mag PP")
    field(LNK1, "$(P)$(R)$(PI_TYPE)TblRampEnd-Mag PP")
}

record(ao, "$(P)$(R)$(PI_TYPE)TblFix-Ang") {
    field(DESC, "Value for fixed angle table")

    info(DESCRIPTION, "Value for fixed angle table")
    field(FLNK, "$(P)$(R)#$(PI_TYPE)TblFixS-Ang")
    field(PREC, "12")
}

record(seq, "$(P)$(R)#$(PI_TYPE)TblFixS-Ang") {
    field(DESC, "Set fixed value ")

    info(DESCRIPTION, "Set fixed value ")
    field(DOL0, "$(P)$(R)$(PI_TYPE)TblFix-Ang")
    field(DOL1, "$(P)$(R)$(PI_TYPE)TblFix-Ang")
    field(LNK0, "$(P)$(R)$(PI_TYPE)TblRampStr-Ang PP")
    field(LNK1, "$(P)$(R)$(PI_TYPE)TblRampEnd-Ang PP")
}
