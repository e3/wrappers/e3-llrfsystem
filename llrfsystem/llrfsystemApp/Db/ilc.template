# ILC Interface

record(bo, "$(P)$(R)ILC-Enable") {
    field(DESC, "Enable/Disable ILC")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(VAL,  "0")
    field(FLNK, "$(P)$(R)#ILC-LckPG")

    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

# This is the real PV to enable/disable the ILC
record(bo, "$(P)$(R)#ILC-Enable") {
    field(DESC, "Internal Enable/Disable ILC")
    field(ZNAM, "Disabled")
    field(ONAM, "Enabled")
    field(VAL,  "0")
}

record(ao, "$(P)$(R)ILC-BW") {
    field(DESC, "Low-pass filter bandwidth")
    field(EGU,  "Hz")
    field(VAL,  "200000")
    field(PREC, "0")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)ILC-Kval") {
    field(DESC, "ILC gain factor")
    field(VAL,  "0.4")
    field(PREC, "4")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)ILC-Shift") {
    field(DESC, "ILC time shift")
    field(VAL,  "0")
    field(PREC, "4")
    field(EGU,  "ms")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)ILC-Start") {
    field(DESC, "ILC window start")
    field(PREC, "4")
    field(VAL,  "0.0005")
    field(EGU,  "ms")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)ILC-End") {
    field(DESC, "ILC window end")
    field(PREC, "4")
    field(VAL,  "2.7")
    field(EGU,  "ms")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ao, "$(P)$(R)ILC-Limit") {
    field(DESC, "ILC magnitude limit")
    field(PREC, "6")
    field(VAL,  "0.999969")

    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(waveform, "$(P)$(R)ILC-PID-Rsp-I") {
    field(DESC, "PID controller time response")
    field(FTVL, "DOUBLE")
    field(NELM, "$(SMNM_MAX)")
}

record(waveform, "$(P)$(R)ILC-PID-Rsp-Q") {
    field(DESC, "PID controller time response")
    field(FTVL, "DOUBLE")
    field(NELM, "$(SMNM_MAX)")
}

# Internal records

# If not in Operation mode it should use the internal IQ
# to initiate the ILC
record(calcout, "$(P)$(R)#ILC-ChkUseIntRB"){
    field(INPA, "$(P)$(R=)Mode-RB")
    field(CALC, "A#4?1:0")
    field(OOPT, "When Non-zero")
    field(OUT,  "$(P)$(R)#ILC-UseIntRB PP")
}

record(calcout, "$(P)$(R)#ILC-LckPG") {
    field(DESC, "Lock/Unlock FF pulse generation")
    field(INPA, "$(P)$(R)ILC-Enable")
    field(CALC, "A ? 15 : 10")
    field(OUT,  "$(P)$(R)#ILC-UpdPG.SELN")
    field(FLNK, "$(P)$(R)#ILC-UpdPG")
}

record(seq, "$(P)$(R)#ILC-UpdPG") {
    field(DESC, "Lock/Unlock FF pulse generation")
    field(SELM, "Mask")
    field(SELN, "0")
    field(SHFT, "0")
    # Stop pulse generation
    field(DOL0, "0")
    field(LNK0, "$(P)$(R)FFPulseGenEn PP")
    # Prevent pulse generation from running when ILC is active
    field(DOL1, "$(P)$(R)ILC-Enable")
    field(LNK1, "$(P)$(R)FFPulseGenEn.DISP")
    field(DO2,  "1")
    field(LNK2, "$(P)$(R)#ILC-ChkUseIntRB.PROC")
    field(DOL3, "$(P)$(R)ILC-Enable")
    field(LNK3, "$(P)$(R)#ILC-Enable PP")
}

record(waveform, "$(P)$(R)#ILC-PID-AB") {
    field(DESC, "PID controller polynomials")
    field(FTVL, "DOUBLE")
    field(NELM, "6")  # [a0, a1, a2, b0, b1, b2]
}

record(waveform, "$(P)$(R)#ILC-LPF-AB") {
    field(DESC, "LP filter polynomials")
    field(FTVL, "DOUBLE")
    field(NELM, "6")  # [a0, a1, a2, b0, b1, b2]
}

record(calc, "$(P)$(R)#ILC-Ts") {
    field(DESC, "Sampling Time")
    field(INPA, "$(PD)$(RD)IQSmpNearIQ-N-RB CP")
    field(INPB, "$(P)$(R)FreqSampling CP")
    field(CALC, "A / B")
    field(EGU,  "us")
    field(PREC, "4")
}

record(calc, "$(P)$(R)#ILC-StartS") {
    field(DESC, "ILC window start (samples)")
    field(INPA, "$(P)$(R)ILC-Start CP")
    field(INPB, "$(P)$(R)#ILC-Ts CP")
    field(CALC, "A * 1000 / B")
}

record(calc, "$(P)$(R)#ILC-EndS") {
    field(DESC, "ILC window end (samples)")
    field(INPA, "$(P)$(R)ILC-End CP")
    field(INPB, "$(P)$(R)#ILC-Ts CP")
    field(CALC, "A * 1000 / B")
}

record(calc, "$(P)$(R)#ILC-ShiftS") {
    field(DESC, "ILC time shift (samples)")
    field(INPA, "$(P)$(R)ILC-Shift CP")
    field(INPB, "$(P)$(R)#ILC-Ts CP")
    field(CALC, "A * 1000 / B")
}

record(ao, "$(P)$(R)#ILC-ExecTime-I") {
    field(DESC, "Execution time")
    field(EGU,  "s")
    field(PREC, "8")
}

record(ao, "$(P)$(R)#ILC-ExecTime-Q") {
    field(DESC, "Execution time")
    field(EGU,  "s")
    field(PREC, "8")
}

record(bo, "$(P)$(R)#ILC-Ready-I") {
    field(DESC, "ILC Data ready")
    field(VAL,  "0")
}

record(bo, "$(P)$(R)#ILC-Ready-Q") {
    field(DESC, "ILC Data ready")
    field(VAL,  "0")
}

record(aSub, "$(P)$(R)#ILC-PID-Calc") {
    field(DESC, "Calculate PID controller polynomials")
    field(SNAM, "ilc_pid_ab")
    field(PINI, "YES")
    # A: Kp
    field(INPA, "$(PD)$(RD)RFCtrlKp CP")
    field(FTA,  "DOUBLE")
    # B: Ki
    field(INPB, "$(PD)$(RD)RFCtrlKi CP")
    field(FTB,  "DOUBLE")
    # C: Kd
    field(INPC, "$(PD)$(RD)RFCtrlKd CP")
    field(FTC,  "DOUBLE")
    # D: Tf
    field(INPD, "$(PD)$(RD)RFCtrlTf CP")
    field(FTD,  "DOUBLE")
    # E: Sample time
    field(INPE, "$(P)$(R)#ILC-Ts CP")
    field(FTE,  "DOUBLE")
    # F: Open loop
    field(INPF, "$(PD)$(RD)OpenLoop-RB CP")
    field(FTF,  "SHORT")
    # OUTA: Polynomial coefficents
    field(OUTA, "$(P)$(R)#ILC-PID-AB PP")
    field(FTVA, "DOUBLE")
    field(NOVA, "6")  # [a0, a1, a2, b0, b1, b2]
}

record(aSub, "$(P)$(R)#ILC-LPF-Calc") {
    field(DESC, "Calculate low-pass filter polynomials")
    field(SNAM, "ilc_lpf_ab")
    field(PINI, "YES")
    # A: Filter bandwidth
    field(INPA, "$(P)$(R)ILC-BW CP")
    field(FTA,  "DOUBLE")
    # B: Sample time
    field(INPB, "$(P)$(R)#ILC-Ts CP")
    field(FTB,  "DOUBLE")
    # Out: Polynomial coefficents
    field(OUTA, "$(P)$(R)#ILC-LPF-AB PP")
    field(FTVA, "DOUBLE")
    field(NOVA, "6")  # [a0, a1, a2, b0, b1, b2]
}

### Starting records for timing integration
#

record(ao, "$(P)$(R)ILC-BeamCurLim"){
    field(DESC, "Beam Cur Lim to restart ILC")
    field(VAL,  "0")
    field(PREC, "6")
    field(EGU,  "mA")

    info(DESCRIPTION, "The limit delta on Beam Current to trigger the restart of AFF")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

record(ai, "$(P)$(R)#ILC-LastBCur"){
    field(DESC, "Last beam current")
    field(VAL,  "-1")
    field(EGU,  "mA")

    info(DESCRIPTION, "Last beam current when ILC was calculated")
}

record(ai, "$(P)$(R)#ILC-LastBLen"){
    field(DESC, "Last beam length")
    field(VAL,  "-1")
    field(EGU,  "ms")

    info(DESCRIPTION, "Last beam length when ILC was calculated")
}

record(ao, "$(P)$(R)ILC-OffBStart"){
    field(DESC, "Offset beam start")
    field(VAL,  "0")
    field(EGU,  "ms")
    field(PREC, "6")

    info(DESCRIPTION, "Offset before beam start")
}

record(ao, "$(P)$(R)ILC-OffBEnd"){
    field(DESC, "Offset beam end")
    field(VAL,  "0")
    field(EGU,  "ms")
    field(PREC, "6")

    info(DESCRIPTION, "Offset after beam end")
}

record(calc, "$(P)$(R)#ILC-BStart"){
    field(DESC, "Beam start when in beam operation")
    field(INPA, "$(P)$(R)BStartPosLLRF")
    field(INPB, "$(P)$(R)ILC-OffBStart")
    field(CALC, "A-B>0?A-B:0")
}

record(calc, "$(P)$(R)#ILC-BEnd"){
    field(DESC, "Beam end when in beam operation")
    field(INPA, "$(P)$(R)EVRBEndLastPulse")
    field(INPB, "$(P)$(R)ILC-OffBEnd")
    field(INPC, "$(PEVR)RFSyncWdt-SP")
    field(CALC, "A+B<C/1000?A+B:C")
}

record(acalcout, "$(P)$(R)#ILC-Proc"){
    field(DESC, "Decides if proc ILC or not")
    field(INAA, "$(PD)$(RD)IntChILCtrl-Cmp0 CP")
    field(INPA, "$(P)$(R)#ILC-Enable")
    field(INPB, "$(P)$(R=)Mode-RB")
    field(INPC, "$(PD)$(RD)TrigILock")
    field(CALC, "A=0||B=4||C=1?0:192")
    field(OUT,  "$(P)$(R)#ILC-ProcFan.SELN")
    field(FLNK, "$(P)$(R)#ILC-Proc2")
}

# This extra record is to avoid unecessary processing
# on ProcFan when in Beam Operation Mode
record(calcout, "$(P)$(R)#ILC-Proc2"){
    field(INPA, "$(P)$(R)#ILC-Proc")
    field(CALC, "A#0?1:0")
    field(OOPT, "When Non-zero")
    field(OUT,  "$(P)$(R)#ILC-ProcFan.PROC")
}

record(seq, "$(P)$(R)#ILC-ProcFan"){
    field(SELM, "Mask")
    field(SHFT, "0")
    # Stores current and beam length
    field(DOL0, "$(P)$(R)EVRBCurrLastPulse")
    field(LNK0, "$(P)$(R)#ILC-LastBCur PP")
    field(DOL1, "$(P)$(R)EVRBLenLastPulse")
    field(LNK1, "$(P)$(R)#ILC-LastBLen PP")
    # Set beam start and beam end
    field(DOL2, "1")
    field(LNK2, "$(P)$(R)#ILC-BStart.PROC")
    field(DOL3, "1")
    field(LNK3, "$(P)$(R)#ILC-BEnd.PROC")
    field(DOL4, "$(P)$(R)#ILC-BStart")
    field(LNK4, "$(P)$(R)ILC-Start PP")
    field(DOL5, "$(P)$(R)#ILC-BEnd")
    field(LNK5, "$(P)$(R)ILC-End PP")
    # Process ILC
    field(DOL6, "1")
    field(LNK6, "$(P)$(R)#ILC-Calc-I.PROC")
    field(DOL7, "1")
    field(LNK7, "$(P)$(R)#ILC-Calc-Q.PROC")
}

## records to use the latest FF as there was no beam on past pulse
# begin
record(calcout, "$(P)$(R)#ILC-SetNUSEILst"){
    field(DESC, "Set NUSE of CopyLst")
    field(INPA, "$(P)$(R)#ILC-OutTbl-I.NORD")
    field(CALC, "A")
    field(OUT,  "$(P)$(R)#ILC-CopyILst.NUSE")
    field(FLNK, "$(P)$(R)#ILC-CopyILst")
}

record(acalcout, "$(P)$(R)#ILC-CopyILst"){
    field(NELM, "$(SMNM_MAX)")
    field(NUSE, "$(SMNM_MAX)")
    field(SIZE, "NUSE")
    field(INAA, "$(P)$(R)#ILC-OutTbl-I")
    field(CALC, "AA")
    field(OUT,  "$(PD)$(RD)FFTbl-I PP")
    field(FLNK, "$(P)$(R)#ILC-SetNUSEQLst")
}

record(calcout, "$(P)$(R)#ILC-SetNUSEQLst"){
    field(DESC, "Set NUSE of CopyQLst")
    field(INPA, "$(P)$(R)#ILC-OutTbl-Q.NORD")
    field(CALC, "A")
    field(OUT,  "$(P)$(R)#ILC-CopyQLst.NUSE")
    field(FLNK, "$(P)$(R)#ILC-CopyQLst")
}

record(acalcout, "$(P)$(R)#ILC-CopyQLst"){
    field(NELM, "$(SMNM_MAX)")
    field(NUSE, "$(SMNM_MAX)")
    field(SIZE, "NUSE")
    field(INAA, "$(P)$(R)#ILC-OutTbl-Q")
    field(CALC, "AA")
    field(OUT,  "$(PD)$(RD)FFTbl-Q PP")
    field(FLNK, "$(P)$(R)#ILC-CntCpTbl")
}

# DEBUG
record(calc, "$(P)$(R)#ILC-CntCpTbl"){
    field(VAL,  "0")
    field(CALC, "VAL+1")
    field(FLNK, "$(P)$(R)#ILC-ChkBeam")
}

### Static compensation records

record(aSub, "$(P)$(R)#ILC-StatComp") {
    field(DESC, "Static Compensation for ILC")
    field(SNAM, "ilc_static_comp")
    # Static feed forward compensation
    field(INPA, "$(P)$(R)BStartPosSmp")
    field(FTA,  "SHORT")
    field(INPB, "$(P)$(R)EVRBEndPosSmp")
    field(FTB,  "SHORT")
    field(INPC, "$(P)$(R)#ILC-AutoStatMag")
    field(INPD, "$(P)$(R)#ILC-AutoStatAng")
    field(INPE, "$(P)$(R)#FFPulseGenTabS")
    field(FTE,  "LONG")
    # Magnitude Array
    field(FTVA, "DOUBLE")
    field(NOVA, "$(SMNM_MAX)")
    field(OUTA, "$(PD)$(RD)FFTbl-Mag PP")
    # Angle Array
    field(FTVB, "DOUBLE")
    field(NOVB, "$(SMNM_MAX)")
    field(OUTB, "$(PD)$(RD)FFTbl-Ang PP")
    field(FLNK, "$(P)$(R)#ILC-StatCompPos")
}

record(seq, "$(P)$(R)#ILC-StatCompPos"){
    field(DESC, "Pos proc stat comp")
    # Increment counter debugger
    field(DOL0, "1")
    field(LNK0, "$(P)$(R)#ILC-CntStatComp.PROC")
    # Write table
    field(DOL1, "1")
    field(LNK1, "$(PD)$(RD)FFTbl-TblToFW PP")
    field(DLY1, "0.01")  # 10 ms
    # Store current and length
    field(DOL2, "$(P)$(R)EVRBCurr")
    field(LNK2, "$(P)$(R)#ILC-LastBCurStatComp PP")
    field(DOL3, "$(P)$(R)EVRBLen")
    field(LNK3, "$(P)$(R)#ILC-LastBLenStatComp PP")
    field(DOL4, "1")
    field(LNK4, "$(P)$(R)#ILC-UseIntRB PP")
}

record(ai, "$(P)$(R)#ILC-UseIntRB"){
    field(DESC, "ILC should use int. RB")
    field(VAL,  "0")
}

# DEBUG
record(calc, "$(P)$(R)#ILC-CntStatComp"){
    field(VAL,  "0")
    field(CALC, "VAL+1")
    field(FLNK, "$(P)$(R)#ILC-Ready")
}

# Store the BCur and BLen from Static compensation
record(ai, "$(P)$(R)#ILC-LastBCurStatComp"){
    field(DESC, "Last beam current static comp.")
    field(VAL,  "-1")
    field(EGU,  "mA")

    info(DESCRIPTION, "Last beam current when static compensation was calculated")
}

record(ai, "$(P)$(R)#ILC-LastBLenStatComp"){
    field(DESC, "Last beam length")
    field(VAL,  "-1")
    field(EGU,  "ms")

    info(DESCRIPTION, "Last beam length when static compensation was calculated")
}

## records to use the latest FF as there was no beam on past pulse
# end

record(aSub, "$(P)$(R)#ILC-Calc-I") {
    field(DESC, "Iterative Learning Control algorithm")
    field(SNAM, "ilc_process")
    # A: Enable/Disable
    field(INPA, "$(P)$(R)#ILC-Enable")
    field(FTA,  "SHORT")
    field(NOA,  "1")
    # B: PID Error
    field(INPB, "$(PD)$(RD)IntChILCtrl-Cmp0")
    field(FTB,  "DOUBLE")
    field(NOB,  "$(SMNM_MAX)")
    # C: FF Table
    field(INPC, "$(P)$(R)#ILC-OutTbl-I")
    field(FTC,  "DOUBLE")
    field(NOC,  "$(SMNM_MAX)")
    # D: Gain factor (K)
    field(INPD, "$(P)$(R)ILC-Kval")
    field(FTD,  "DOUBLE")
    field(NOD,  "1")
    # E: ILC start (sample index)
    field(INPE, "$(P)$(R)#ILC-StartS")
    field(FTE,  "ULONG")
    field(NOE,  "1")
    # F: ILC end (sample index)
    field(INPF, "$(P)$(R)#ILC-EndS")
    field(FTF,  "ULONG")
    field(NOF,  "1")
    # G: Time shift (samples)
    field(INPG, "$(P)$(R)#ILC-ShiftS")
    field(FTG,  "LONG")
    field(NOG,  "1")
    # H: PID controller polynomials A, B
    field(INPH, "$(P)$(R)#ILC-PID-AB")
    field(FTH,  "DOUBLE")
    field(NOH,  "6")
    # I: LP filter polynomials A, B
    field(INPI, "$(P)$(R)#ILC-LPF-AB")
    field(FTI,  "DOUBLE")
    field(NOI,  "6")
    # J: Output limit
    field(INPJ, "$(P)$(R)ILC-Limit")
    field(FTJ,  "DOUBLE")
    field(NOJ,  "1")
    # K: Uses internal readback
    field(INPK, "$(P)$(R)#ILC-UseIntRB")
    field(FTK,  "SHORT")
    field(NOK,  "1")
    # L: Internal RB
    field(INPL, "$(PD)$(RD)#FFTbl-I-RB")
    field(FTL,  "DOUBLE")
    field(NOL,  "$(SMNM_MAX)")
    # OUTA: Updated FF table
    field(OUTA, "$(PD)$(RD)FFTbl-I PP")
    field(FTVA, "DOUBLE")
    field(NOVA, "$(SMNM_MAX)")
    # OUTB: Save Tbl-I for the next pulse
    field(OUTB, "$(P)$(R)#ILC-OutTbl-I PP")
    field(FTVB, "DOUBLE")
    field(NOVB, "$(SMNM_MAX)")
    # OUTC: PID response
    field(OUTC, "$(P)$(R)ILC-PID-Rsp-I PP")
    field(FTVC, "DOUBLE")
    field(NOVC, "$(SMNM_MAX)")
    # OUTD: Execution time
    field(OUTD, "$(P)$(R)#ILC-ExecTime-I PP")
    field(FTVD, "DOUBLE")
    field(NOVD, "1")
    # OUTE: Ready
    field(OUTE, "$(P)$(R)#ILC-Ready-I PP")
    field(FTVE, "SHORT")
    field(NOVE, "1")
    field(FLNK, "$(P)$(R)#ILC-Ready.PROC")
#    field(TPRO, "1")
}

record(aSub, "$(P)$(R)#ILC-Calc-Q") {
    field(DESC, "Iterative Learning Control algorithm")
    field(SNAM, "ilc_process")
    # A: Enable/Disable
    field(INPA, "$(P)$(R)#ILC-Enable")
    field(FTA,  "SHORT")
    field(NOA,  "1")
    # B: PID Error
    field(INPB, "$(PD)$(RD)IntChILCtrl-Cmp1")
    field(FTB,  "DOUBLE")
    field(NOB,  "$(SMNM_MAX)")
    # C: FF Table
    field(INPC, "$(P)$(R)#ILC-OutTbl-Q")
    field(FTC,  "DOUBLE")
    field(NOC,  "$(SMNM_MAX)")
    # D: Gain factor (K)
    field(INPD, "$(P)$(R)ILC-Kval")
    field(FTD,  "DOUBLE")
    field(NOD,  "1")
    # E: ILC start (sample index)
    field(INPE, "$(P)$(R)#ILC-StartS")
    field(FTE,  "ULONG")
    field(NOE,  "1")
    # F: ILC end (sample index)
    field(INPF, "$(P)$(R)#ILC-EndS")
    field(FTF,  "ULONG")
    field(NOF,  "1")
    # G: Time shift (samples)
    field(INPG, "$(P)$(R)#ILC-ShiftS")
    field(FTG,  "LONG")
    field(NOG,  "1")
    # H: PID controller polynomials A, B
    field(INPH, "$(P)$(R)#ILC-PID-AB")
    field(FTH,  "DOUBLE")
    field(NOH,  "6")
    # I: LP filter polynomials A, B
    field(INPI, "$(P)$(R)#ILC-LPF-AB")
    field(FTI,  "DOUBLE")
    field(NOI,  "6")
    # J: Output limit
    field(INPJ, "$(P)$(R)ILC-Limit")
    field(FTJ,  "DOUBLE")
    field(NOJ,  "1")
    # K: Uses internal readback
    field(INPK, "$(P)$(R)#ILC-UseIntRB")
    field(FTK,  "SHORT")
    field(NOK,  "1")
    # L: Internal RB
    field(INPL, "$(PD)$(RD)#FFTbl-Q-RB")
    field(FTL,  "DOUBLE")
    field(NOL,  "$(SMNM_MAX)")
    # OUTA: Updated FF table
    field(OUTA, "$(PD)$(RD)FFTbl-Q PP")
    field(FTVA, "DOUBLE")
    field(NOVA, "$(SMNM_MAX)")
    # OUTB: Save Tbl-Q for the next pulse
    field(OUTB, "$(P)$(R)#ILC-OutTbl-Q PP")
    field(FTVB, "DOUBLE")
    field(NOVB, "$(SMNM_MAX)")
    # OUTC: PID response
    field(OUTC, "$(P)$(R)ILC-PID-Rsp-Q PP")
    field(FTVC, "DOUBLE")
    field(NOVC, "$(SMNM_MAX)")
    # OUTD: Execution time
    field(OUTD, "$(P)$(R)#ILC-ExecTime-Q PP")
    field(FTVD, "DOUBLE")
    field(NOVD, "1")
    # OUTE: Ready
    field(OUTE, "$(P)$(R)#ILC-Ready-Q PP")
    field(FTVE, "SHORT")
    field(NOVE, "1")
    field(FLNK, "$(P)$(R)#ILC-CntILC")
}

# DEBUG
record(calc, "$(P)$(R)#ILC-CntILC"){
    field(VAL,  "0")
    field(CALC, "VAL+1")
    field(FLNK, "$(P)$(R)#ILC-Ready")
}

record(calcout, "$(P)$(R)#ILC-Ready") {
    field(DESC, "ILC data ready")
    field(INPA, "$(P)$(R)#ILC-Ready-I")
    field(INPB, "$(P)$(R)#ILC-Ready-Q")
    field(CALC, "A && B ? 1 : 0")
    field(OOPT, "When Non-zero")
    field(OUT,  "$(P)$(R)#ILC-ChkBeam.PROC")
}

## When in Beam Operation mode only produces FF if there will be beam
record(calcout, "$(P)$(R)#ILC-ChkBeam"){
    field(INPA, "$(P)$(R=)Mode-RB")
    field(INPB, "$(P)$(R)EVRBeamNextPulse")
    field(INPC, "$(P)$(R)ILC-Enable")
    field(CALC, "C=0?0:(A#4?23:(A=4&&B=1?23:31))")
    field(OUT,  "$(P)$(R)#ILC-Write-FF.SELN")
    field(FLNK, "$(P)$(R)#ILC-Write-FF")
}

record(seq, "$(P)$(R)#ILC-Write-FF") {
    field(DESC, "Write FF Tables")
    field(SELM, "Mask")
    field(SELN, "11")
    field(SHFT, "0")
    # After running use internal should always go to 0
    field(DOL0, "0")
    field(LNK0, "$(P)$(R)#ILC-UseIntRB PP")
    field(DOL1, "0")
    field(LNK1, "$(P)$(R)#ILC-Ready-I PP")
    field(DOL2, "0")
    field(LNK2, "$(P)$(R)#ILC-Ready-Q PP")
    # if there is no beam produces FF with 0
    field(DOL3, "1")
    field(LNK3, "$(P)$(R)#ILC-ZeroFFI.PROC")
    field(DOL4, "1")
    field(LNK4, "$(PD)$(RD)FFTbl-TblToFW PP")
    field(DLY4, "0.01")  # 10 ms
    field(FLNK, "$(P)$(R)#ILC-CntWrt")
}

# We should fill all the table otherwise it might
# not go to zero (it seems to be an issue)
record(acalcout, "$(P)$(R)#ILC-ZeroFFI"){
    field(INAA, "[0]")
    field(CALC, "AA*$(SMNM_MAX)")
    field(NELM, "$(SMNM_MAX)")
    field(OUT,  "$(PD)$(RD)FFTbl-I PP")
    field(FLNK, "$(P)$(R)#ILC-ZeroFFQ")
}

record(acalcout, "$(P)$(R)#ILC-ZeroFFQ"){
    field(INAA, "[0]")
    field(CALC, "AA*$(SMNM_MAX)")
    field(NELM, "$(SMNM_MAX)")
    field(OUT,  "$(PD)$(RD)FFTbl-Q PP")
    field(FLNK, "$(P)$(R)#ILC-ZeroFF")
}

# DEBUG

record(calc, "$(P)$(R)#ILC-ZeroFF"){
    field(VAL,  "0")
    field(CALC, "VAL+1")
}

record(calc, "$(P)$(R)#ILC-CntWrt"){
    field(VAL,  "0")
    field(CALC, "VAL+1")
}

record(waveform, "$(P)$(R)#ILC-OutTbl-I") {
    field(DESC, "ILC output Tbl I")
    field(FTVL, "DOUBLE")
    field(NELM, "$(SMNM_MAX)")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(INP,  "[0]")
    field(PINI, "YES")

    info(DESCRIPTION, "Last output of ILC table I")
    info(SYSTEM,	"LLRF")
}

record(waveform, "$(P)$(R)#ILC-OutTbl-Q") {
    field(DESC, "ILC output Tbl Q")
    field(FTVL, "DOUBLE")
    field(NELM, "$(SMNM_MAX)")
    field(STAT, "NO_ALARM")
    field(SEVR, "NO_ALARM")
    field(INP,  "[0]")
    field(PINI, "YES")

    info(DESCRIPTION, "Last output of ILC table Q")
    info(SYSTEM,	"LLRF")
}

record(fanout, "$(P)$(R)#ILC-StatCompPre"){
    field(DESC, "Pre steps stat comp")
    field(LNK0, "$(P)$(R)#ILC-AutoStatMag")
    field(LNK1, "$(P)$(R)#ILC-AutoStatAng")
    field(LNK2, "$(P)$(R)#ILC-StatComp")
}

record(calc, "$(P)$(R)#ILC-AutoStatMag"){
    field(DESC, "calc. auto. stat. mag")
    field(INPA, "$(P)$(R)FFAutoStatCompEn")
    field(INPB, "$(P)$(R)EVRBCurr")
    field(INPC, "$(P)$(R)EVRBeamNextPulse")
    field(INPD, "$(P)$(R)FFAutoStatCompK")
    field(CALC, "A=1&&C=1?B*D:0")

    info(DESCRIPTION, "Calculate auto static compensation for magnitude")
}

record(calc, "$(P)$(R)#ILC-AutoStatAng"){
    field(DESC, "calc. auto. stat. ang")
    field(INPA, "$(P)$(R)FFAutoStatCompEn")
    field(INPB, "$(P)$(R)EVRBCurr")
    field(INPC, "$(P)$(R)EVRBeamNextPulse")
    field(INPD, "$(P)$(R)FFAutoStatCompX")
    field(CALC, "A=1&&C=1?B*D:0")

    info(DESCRIPTION, "Calculate auto static compensation for angle")
}
