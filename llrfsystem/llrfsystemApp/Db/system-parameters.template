########
# LLRF System Setting - common to entire aggration of LLRF components.
########

record(ai, "$(P)$(R=)IsSCL") {
    field(DESC, "Indicate if system is in SCL")
    field(VAL,  "$(IS_SCL=0)")
}

record(ai, "$(P)$(R=)FreqSampling") {
    field(DESC, "System - Samping Frequency")
    field(VAL,  "$(F-SAMPLING)")
    field(DISP, "1")
    field(PREC, "3")
    field(EGU,  "MHz")
}

record(ai, "$(P)$(R=)FreqSystem") {
    field(DESC, "System - Frequency")
    field(VAL,  "$(F-SYSTEM)")
    field(DISP, "1")
    field(PREC, "3")
    field(EGU,  "MHz")
}

record(mbbi, "$(P)$(R=)MainDig-RB") {
    field(DESC, "Main digitizer")

    info(DESCRIPTION, "Main digitizer")
    field(ONST, "1st Digitizer")
    field(TWST, "2nd Digitizer")
    field(THST, "3rd Digitizer")
    field(FRST, "4th Digitizer")
    field(VAL,  "1")
    field(PINI, "YES")
}

record(ai, "$(P)$(R=)RFStationID-RB") {
    field(DESC, "RF Station ID")
    field(VAL,  "$(RFSTID)")
    field(DISP, "1")
    field(PREC, "0")
}

# Records to indentify which board is the main

record(calcout, "$(P)$(R=)#DetecMainChange1") {
    field(DESC, "Detect main board change")

    info(DESCRIPTION, "Detect main board change")
    field(INPA, "$(DIG1MAIN=$(PD)$(RD)Main-RB CP)")
    field(CALC, "A=1?1:0")
    field(OOPT, "When Non-zero")
    field(OUT,  "$(P)$(R=)MainDig-RB PP")
}

record(calcout, "$(P)$(R=)#DetecMainChange2") {
    field(DESC, "Detect main board change")

    info(DESCRIPTION, "Detect main board change")
    field(INPA, "$(DIG2MAIN=0)")
    field(CALC, "A=1?2:0")
    field(OOPT, "When Non-zero")
    field(OUT,  "$(P)$(R=)MainDig-RB PP")
}

record(calcout, "$(P)$(R=)#DetecMainChange3") {
    field(DESC, "Detect main board change")

    info(DESCRIPTION, "Detect main board change")
    field(INPA, "$(DIG3MAIN=0)")
    field(CALC, "A=1?3:0")
    field(OOPT, "When Non-zero")
    field(OUT,  "$(P)$(R=)MainDig-RB PP")
}

record(calcout, "$(P)$(R=)#DetecMainChange4") {
    field(DESC, "Detect main board change")

    info(DESCRIPTION, "Detect main board change")
    field(INPA, "$(DIG4MAIN=0)")
    field(CALC, "A=1?4:0")
    field(OOPT, "When Non-zero")
    field(OUT,  "$(P)$(R=)MainDig-RB PP")
}

#####
# Operation Modes
#####

record(mbbo, "$(P)$(R=)Mode") {
    field(DESC, "Operation Mode")
    field(ZRST, "LLRF Expert")
    field(ONST, "RF Conditioning")
    field(TWST, "Cavity Conditioning")
    field(THST, "$(RAMP_UP_MODE)")
    field(FRST, "Beam Operation")
    field(FVST, "Beam Operation Special")
    field(VAL,  "0")
    field(PINI, "RUNNING")

    info(DESCRIPTION, "Operation Mode")
    info(autosaveFields, "VAL")
    info(SAVRES_THIS, "")
    info(ARCHIVE_THIS, "")
}

# Records for automatic behaviour and restrictions for operation mode

record(calcout, "$(P)$(R)#ChkMode") {
    field(DESC, "Check operation mode")
    field(CALC, "A=4?(B=2&C=0&D=4?4:3):A")
    field(INPA, "$(P)$(R=)Mode CP")
    field(INPB, "$(PEVR)OpMode CP")
    field(INPC, "$(PD)$(RD)OpenLoop-RB CP")
    field(INPD, "$(PD)$(RD)FSM CP")
    field(OUT,  "$(P)$(R=)Mode-RB PP")
}

record(mbbi, "$(P)$(R=)Mode-RB") {
    field(DESC, "Operation Mode readback")
    field(ZRST, "LLRF Expert")
    field(ONST, "RF Conditioning")
    field(TWST, "Cavity Conditioning")
    field(THST, "$(RAMP_UP_MODE)")
    field(FRST, "Beam Operation")
    field(FVST, "Beam Operation Special")
    field(FLNK, "$(P)$(R)#PrepModeAutoBhv PP")
}

record(calcout, "$(P)$(R)#PrepModeAutoBhv") {
    field(DESC, "Prepare mode auto behaviour")
    field(CALC, "A")
    field(INPA, "$(P)$(R=)Mode-RB")
    field(OUT,  "$(P)$(R)#ModeAutoBhv.SELN")
    field(FLNK, "$(P)$(R)#ModeAutoBhv")
}

record(fanout, "$(P)$(R)#ModeAutoBhv") {
    field(SELM, "Specified")
    field(LNK0, "$(P)$(R)#LLRFExpertAutoBhv")
    field(LNK1, "$(P)$(R)#RFCondBeamOpAutoBhv")
    field(LNK2, "$(P)$(R)#CalCavCondAutoBhv")
    field(LNK3, "$(P)$(R)#RampUpAutoBhv")
    field(LNK4, "$(P)$(R)#BeamOpAutoBhv")
    field(LNK5, "$(P)$(R)#BeamOpSpAutoBhv")
}

record(seq, "$(P)$(R)#LLRFExpertAutoBhv") {
    field(DESC, "LLRF Expert auto behaviour")
    field(SELM, "All")
    field(DOL0, "0")
    field(LNK0, "$(P)$(R)ILC-Enable PP")
    field(DOL1, "1")
    field(LNK1, "$(P)$(R)#CalibLockSet.SELN")
    field(DOL2, "1")
    field(LNK2, "$(P)$(R)#CalibLockSet.PROC")
    field(DOL3, "0")
    field(LNK3, "$(P)$(R)#OutMagLim-UpLimSet.SELN")
    field(DOL4, "1")
    field(LNK4, "$(P)$(R)#OutMagLim-UpLimSet.PROC")
    field(DOL5, "$(DEFAULT_MAX_RFWDT=10000)")
    field(LNK5, "$(P)$(R)RFWdt-UpLim")
}

record(seq, "$(P)$(R)#RFCondBeamOpAutoBhv") {
    field(DESC, "RF Cond auto behaviour")
    field(SELM, "All")
    field(DOL0, "0")
    field(LNK0, "$(P)$(R)ILC-Enable PP")
    field(DOL1, "2")
    field(LNK1, "$(P)$(R)#CalibLockSet.SELN")
    field(DOL2, "1")
    field(LNK2, "$(P)$(R)#CalibLockSet.PROC")
    field(DOL3, "1")
    field(LNK3, "$(P)$(R)#OutMagLim-UpLimSet.SELN")
    field(DOL4, "1")
    field(LNK4, "$(P)$(R)#OutMagLim-UpLimSet.PROC")
    field(DOL5, "$(DEFAULT_MAX_RFWDT=10000)")
    field(LNK5, "$(P)$(R)RFWdt-UpLim")
    field(DOL6, "1")
    field(LNK6, "$(P)$(R)#FreqTrackAutoBhv.PROC")
}

record(calcout, "$(P)$(R)#CalCavCondAutoBhv") {
    field(DESC, "Calc Cav cond auto behaviour")
    field(CALC, "A?$(SCL_MASK=253):$(NCL_MASK=251)")
    field(INPA, "$(P)$(R)IsSCL")
    field(OUT,  "$(P)$(R)#CavCondAutoBhv.SELN")
    field(FLNK, "$(P)$(R)#CavCondAutoBhv")
}

record(seq, "$(P)$(R)#CavCondAutoBhv") {
    field(DESC, "Cav Cond auto behaviour")
    field(SELM, "Mask")
    field(DOL1, "0")
    field(LNK1, "$(P)$(R)ILC-Enable PP")
    # Calibration disable for NCL
    field(DOL2, "2")
    field(LNK2, "$(P)$(R)#CalibLockSet.SELN")
    # Different behaviour for SCL
    field(DOL3, "3")
    field(LNK3, "$(P)$(R)#CalibLockSet.SELN")
    field(DOL4, "1")
    field(LNK4, "$(P)$(R)#CalibLockSet.PROC")
    field(DOL5, "2")
    field(LNK5, "$(P)$(R)#OutMagLim-UpLimSet.SELN")
    field(DOL6, "1")
    field(LNK6, "$(P)$(R)#OutMagLim-UpLimSet.PROC")
    field(DOL7, "$(CAVCOND_MAX_RFWDT=10000)")
    field(DOL7, "$(P)$(R)RFWdt-UpLim")
    field(DOL8, "1")
    field(LNK8, "$(P)$(R)#FreqTrackAutoBhv.PROC")
}

record(seq, "$(P)$(R)#RampUpAutoBhv") {
    field(DESC, "Ramp Up auto behaviour")
    field(SELM, "All")
    field(DOL0, "0")
    field(LNK0, "$(P)$(R)ILC-Enable PP")
    field(DOL1, "2")
    field(LNK1, "$(P)$(R)#CalibLockSet.SELN")
    field(DOL2, "1")
    field(LNK2, "$(P)$(R)#CalibLockSet.PROC")
    field(DOL3, "3")
    field(LNK3, "$(P)$(R)#OutMagLim-UpLimSet.SELN")
    field(DOL4, "1")
    field(LNK4, "$(P)$(R)#OutMagLim-UpLimSet.PROC")
    field(DOL5, "$(DEFAULT_MAX_RFWDT=10000)")
    field(LNK5, "$(P)$(R)RFWdt-UpLim")
    field(DOL6, "1")
    field(LNK6, "$(P)$(R)#FreqTrackAutoBhv.PROC")
}

record(seq, "$(P)$(R)#BeamOpAutoBhv") {
    field(DESC, "Beam Op auto behaviour")
    field(SELM, "All")
    field(DOL0, "1")
    field(LNK0, "$(P)$(R)ILC-Enable PP")
    field(DOL1, "2")
    field(LNK1, "$(P)$(R)#CalibLockSet.SELN")
    field(DOL2, "1")
    field(LNK2, "$(P)$(R)#CalibLockSet.PROC")
    field(DOL3, "4")
    field(LNK3, "$(P)$(R)#OutMagLim-UpLimSet.SELN")
    field(DOL4, "1")
    field(LNK4, "$(P)$(R)#OutMagLim-UpLimSet.PROC")
    field(DOL5, "$(DEFAULT_MAX_RFWDT=10000)")
    field(LNK5, "$(P)$(R)RFWdt-UpLim")
    field(DOL6, "1")
    field(LNK6, "$(P)$(R)#FreqTrackAutoBhv.PROC")
}

record(seq, "$(P)$(R)#BeamOpSpAutoBhv") {
    field(DESC, "Beam Op special auto behaviour")
    field(SELM, "All")
    field(DOL0, "1")
    field(LNK0, "$(P)$(R)ILC-Enable PP")
    field(DOL1, "2")
    field(LNK1, "$(P)$(R)#CalibLockSet.SELN")
    field(DOL2, "1")
    field(LNK2, "$(P)$(R)#CalibLockSet.PROC")
    field(DOL3, "5")
    field(LNK3, "$(P)$(R)#OutMagLim-UpLimSet.SELN")
    field(DOL4, "1")
    field(LNK4, "$(P)$(R)#OutMagLim-UpLimSet.PROC")
    field(DOL5, "$(DEFAULT_MAX_RFWDT=10000)")
    field(LNK5, "$(P)$(R)RFWdt-UpLim")
    field(DOL6, "1")
    field(LNK6, "$(P)$(R)#FreqTrackAutoBhv.PROC")
}

record(seq, "$(P)$(R)#OutMagLim-UpLimSet") {
    field(DESC, "Set upper lim for output mag limiter")
    field(SELM, "Specified")
    # No limit when value 0
    field(DOL0, "0")
    field(LNK0, "$(P)$(R)#OutMagLim-UpLim PP")
    field(DOL1, "0")
    field(LNK1, "$(P)$(R)#OutMagLim-UpLim PP")
    # Diffrerent limit for section
    field(DOL2, "$(CAV_COND_MAGLIM)")
    field(LNK2, "$(P)$(R)#OutMagLim-UpLim PP")
    field(DOL3, "$(RAMP_UP_MAGLIM)")
    field(LNK3, "$(P)$(R)#OutMagLim-UpLim PP")
    field(DOL4, "$(BEAM_OP_MAGLIM)")
    field(LNK4, "$(P)$(R)#OutMagLim-UpLim PP")
    field(DOL5, "$(BEAM_OP_SPEC_MAGLIM)")
    field(LNK5, "$(P)$(R)#OutMagLim-UpLim PP")
}

record(ao, "$(P)$(R)#OutMagLim-UpLim") {
    field(DESC, "Upper limit for out magnitude limit")
    field(FLNK, "$(P)$(R)#OutMagLim-UpLimSetAll PP")
}

record(seq, "$(P)$(R)#CalibLockSet") {
    field(DESC, "Set calib auto behaviour mask")
    field(SELM, "Specified")
    field(DOL1, "$(CAL_UNLOCK=9)")
    field(LNK1, "$(P)$(R)#CalibLockMask PP")
    field(DOL2, "$(CAL_LOCK=18)")
    field(LNK2, "$(P)$(R)#CalibLockMask PP")
    field(DOL3, "$(CAV_CAL_LOCK=20)")
    field(LNK3, "$(P)$(R)#CalibLockMask PP")
}

record(ao, "$(P)$(R)#CalibLockMask") {
    field(DESC, "Mask to select auto behaviour")
    field(FLNK, "$(P)$(R)#CalibLockAutoBhv")
}

record(calc, "$(P)$(R)#CalDigMask") {
    field(DESC, "Calc mask according to digs")
    field(CALC, "2^A-1")
    field(INPA, "$(NBOARDS=1)")
    field(FLNK, "$(P)$(R)#DigMask")
    field(PINI, "YES")
}

record(seq, "$(P)$(R)#DigMask") {
    field(SELM, "All")
    field(DOL1, "$(P)$(R)#CalDigMask")
    field(LNK1, "$(P)$(R)#OutMagLim-UpLimSetAll.SELN")
    field(DOL2, "1")
    field(LNK2, "$(P)$(R)#CalLockDigMask.PROC")
}

record(calcout, "$(P)$(R)#CalLockDigMask") {
    field(DESC, "Mask to lock and proc all digs")
    field(CALC, "17*A")
    field(INPA, "$(P)$(R)#CalDigMask")
    field(OUT,  "$(P)$(R)#CalibLockAutoBhv.SELN")
}

record(seq, "$(P)$(R)#CalibLockAutoBhv") {
    field(DESC, "Lock calib for digitizers")
    field(SELM, "Mask")
    field(LNK1, "$(PD)$(LLRF_DIG_R_1)#CalibLockAutoBhv.SELN")
    field(LNK2, "$(PD)$(LLRF_DIG_R_2)#CalibLockAutoBhv.SELN")
    field(LNK3, "$(PD)$(LLRF_DIG_R_3)#CalibLockAutoBhv.SELN")
    field(LNK4, "$(PD)$(LLRF_DIG_R_4)#CalibLockAutoBhv.SELN")
    field(LNK5, "$(PD)$(LLRF_DIG_R_1)#CalibLockAutoBhv.PROC")
    field(LNK6, "$(PD)$(LLRF_DIG_R_2)#CalibLockAutoBhv.PROC")
    field(LNK7, "$(PD)$(LLRF_DIG_R_3)#CalibLockAutoBhv.PROC")
    field(LNK8, "$(PD)$(LLRF_DIG_R_4)#CalibLockAutoBhv.PROC")
    field(DOL1, "$(P)$(R)#CalibLockMask")
    field(DOL2, "$(P)$(R)#CalibLockMask")
    field(DOL3, "$(P)$(R)#CalibLockMask")
    field(DOL4, "$(P)$(R)#CalibLockMask")
    field(DOL5, "1")
    field(DOL6, "1")
    field(DOL7, "1")
    field(DOL8, "1")
}

record(seq, "$(P)$(R)#OutMagLim-UpLimSetAll") {
    field(DESC, "Set upper limit for all mag out limit")
    field(SELM, "Mask")
    field(DOL1, "$(P)$(R)#OutMagLim-UpLim")
    field(LNK1, "$(PD)$(LLRF_DIG_R_1)OutMagLim-UpLim PP")
    field(DOL2, "$(P)$(R)#OutMagLim-UpLim")
    field(LNK2, "$(PD)$(LLRF_DIG_R_2)OutMagLim-UpLim PP")
    field(DOL3, "$(P)$(R)#OutMagLim-UpLim")
    field(LNK3, "$(PD)$(LLRF_DIG_R_3)OutMagLim-UpLim PP")
    field(DOL4, "$(P)$(R)#OutMagLim-UpLim")
    field(LNK4, "$(PD)$(LLRF_DIG_R_4)OutMagLim-UpLim PP")
}
