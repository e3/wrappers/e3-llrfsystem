# Load specific variables
# MACRO SEC: accelerator section (rfq, mebt, dtl, spoke, mbl, hbl, ts2, generic)

iocshLoad("$(llrfsystem_DIR)/$(SEC=generic)_vars.iocsh")

# Common variables
epicsEnvSet("LOG_SERVER_NAME",          "172.30.4.43")
epicsEnvSet("EPICS_CA_MAX_ARRAY_BYTES", "16777300")
epicsEnvSet("TABLE_SMNM_MAX",           "10000")
epicsEnvSet("RFENDPV",                  "$(PD)$(RD)PosRFEnd")
epicsEnvSet("SP_SMNM_MAX",              "0x4000")
epicsEnvSet("FF_SMNM_MAX",              "0x10000")
epicsEnvSet("DEFAULT_EGU",               "ADC Counts")
epicsEnvSet("DEFAULT_EGU_FF",            "ADC Counts")

require sis8300llrf, $(SIS8300LLRF_VER=)

ndsSetTraceLevel 2

# Needed to avoid problems on the callback queue
callbackSetQueueSize(100000)

# Records to estimate gain
dbLoadRecords("gains.template", "P=$(P), R=$(R), PD=$(PD), RDPREAMP=$(RDPREAMP=$(RD)), RDPWRAMP=$(RDPWRAMP=$(RD)), RDVM=$(RDVM=$(RD)), CHPREAMP=$(CHPREAMP=2), CHPWRAMP=$(CHPWRAMP=3), CHVM=$(CHVM=7)")

# Klystron Records
$(KLYDB=#)dbLoadRecords("kly_info.template", "P=$(PKLY=), R=$(RKLY=)")

# Load sis8300llrf snippet - for SCL we set extra unit and different ramp up mode name
$(SCL_TS2=#)epicsEnvSet("NCL", "#")
$(SCL_TS2=#)epicsEnvSet("EGU0_NO_GRAD_EN", "$(EGU0_NO_GRAD_EN=W)")
$(SCL_TS2=#)epicsEnvSet("RAMP_UP_MODE", "Piezo Compensation")
$(NCL=)epicsEnvSet("RAMP_UP_MODE", "Cavity Warm Up")
$(SCL=#)epicsEnvSet("IS_SCL", 1)

dbLoadRecords("system-parameters.template", "P=$(P), R=$(R=), PD=$(PD), RD=$(RD), PEVR=$(PEVR=$(P)RFS-EVR-$(IDX=101):), IS_SCL=$(IS_SCL=0), RAMP_UP_MODE=$(RAMP_UP_MODE), DIG2MAIN=$(DIG2MAIN=0), DIG3MAIN=$(DIG3MAIN=0), DIG4MAIN=$(DIG4MAIN=0), F-SAMPLING=$(F-SAMPLING), F-SYSTEM=$(F-SYSTEM), RFSTID=$(RFSTID=999), LLRF_DIG_R_1=$(LLRF_DIG_R_1=):, LLRF_DIG_R_2=$(LLRF_DIG_R_2=):, LLRF_DIG_R_3=$(LLRF_DIG_R_3=):, LLRF_DIG_R_4=$(LLRF_DIG_R_4=):, CAV_COND_MAGLIM=$(CAV_COND_MAGLIM=0), RAMP_UP_MAGLIM=$(RAMP_UP_MAGLIM=0), BEAM_OP_MAGLIM=$(BEAM_OP_MAGLIM=0), BEAM_OP_SPEC_MAGLIM=$(BEAM_OP_SPEC_MAGLIM=0)")

$(SCL_TS2=#)iocshLoad("$(sis8300llrf_DIR)/sis8300llrf-$(NBOARDS=1)-boards.iocsh", "LPSEN1=$(LPSEN=1), DEFAULT_EGU=$(DEFAULT_EGU), DEFAULT_EGU_FF=$(DEFAULT_EGU_FF), EGU0_NO_GRAD_EN=$(EGU0_NO_GRAD_EN=$(DEFAULT_EGU))")
$(NCL=)iocshLoad("$(sis8300llrf_DIR)/sis8300llrf-$(NBOARDS=1)-boards.iocsh", "LPSEN1=$(LPSEN=1), DEFAULT_EGU=$(DEFAULT_EGU), DEFAULT_EGU_FF=$(DEFAULT_EGU_FF)")

dbLoadRecords("detune.template", "P=$(P), R=$(R), PD=$(PD), RD=$(RD), CAVITYCH=$(CAVITYCH), CAVFWDCH=$(CAVFWDCH), RFENDPV=$(RFENDPV)")

##### Support records to calibration management
# Records to generate ramps for SP and FF
# TODO change for substitution files?
dbLoadRecords("gen-pi-table.template", "P=$(P), R=$(R), PD=$(PD), RD=$(RD), PI_TYPE=FF, PITABLE_SMNM_MAX=$(FF_SMNM_MAX)")

# Automatic calibration for FF
dbLoadRecords("ff-autocalib.template", "P=$(P), R=$(R), PD=$(PD), RD=$(RD), TABLE_SMNM_MAX=$(TABLE_SMNM_MAX)")
dbLoadRecords("ff-autocalib-stats.template", "P=$(P), R=$(R), PD=$(PD), RD=$(RD), TABLE_SMNM_MAX=$(TABLE_SMNM_MAX), PWRFWDCH=$(PWRFWDCH)")
seq("ffautocalib", "P=$(P), R=$(R), PD=$(PD), RD=$(RD), TABLE_SMNM_MAX=$(TABLE_SMNM_MAX)")

# Frequency tracking record
dbLoadRecords("pulse-gen.template", "P=$(P), R=$(R), PD=$(PD), RD=$(RD),PI_TYPE=FF, TABLE_SMNM_MAX=$(FF_SMNM_MAX), RR=$(RR), PVRFWDT=$(PVRFWDT=$(PEVR=$(P)RFS-EVR-$(IDX=101):)RFSyncWdt-SP)")
dbLoadRecords("sp-ramping.template", "P=$(P), R=$(R), PD=$(PD), RD=$(RD), TABLE_SMNM_MAX=$(FF_SMNM_MAX), RR=$(RR), PVRFWDT=$(PVRFWDT=$(PEVR=$(P)RFS-EVR-$(IDX=101):)RFSyncWdt-SP)")

# Automatic static compensation
dbLoadRecords("auto-ffcomp.template", "P=$(P), R=$(R), PI_TYPE=FF")

# Iterative learning control
dbLoadRecords("ilc.template", "P=$(P), R=$(R), PD=$(PD), RD=$(RD), SMNM_MAX=$(FF_SMNM_MAX), PEVR=$(PEVR=$(P)RFS-EVR-$(IDX=101):)")

# timing integration
dbLoadRecords("timing_integration.template", "P=$(P), R=$(R), PD=$(PD), RD=$(RD), DATABUFFER=$(DATABUFFER=$(P)RFS-EVR-$(IDX=101):dbus-recv-u32), PEVR=$(PEVR=$(P)RFS-EVR-$(IDX=101):)")

# Extra board configuration (FF and SP EGU configuration)
iocshLoad("$(llrfsystem_DIR)/config_boards.iocsh", "EGU_FF=$(EGU_FF=$(DEFAULT_EGU_FF))")

# Restore autosaved values
dbLoadRecords("llrfRestoreDetune.template", "P=$(P), R=$(R)")
dbLoadRecords("llrfRestore.template", "P=$(P), R=$(R)")
afterInit("dbpf $(P)$(R)#RestoreLLRF 1")

# Reload last tables
afterInit("dbpf $(PD)$(RD)FFTbl-Mag.PROC 1")
afterInit("dbpf $(PD)$(RD)FFTbl-Ang.PROC 1")
afterInit("dbpf $(PD)$(RD)FFTbl-TblToFW 1")

afterInit("dbpf $(PD)$(RD)SPTbl-Mag.PROC 1")
afterInit("dbpf $(PD)$(RD)SPTbl-Ang.PROC 1")
afterInit("dbpf $(PD)$(RD)SPTbl-TblToFW 1")

afterInit("dbpf $(P)$(R)#FFPulseGen.DISA 0") # Enable FreqTrack Pulse Gen
afterInit("dbpf $(P)$(R)#FFFreqTrack.DISA 0") # Enable FreqTrack
afterInit("dbpf $(P)$(R)#SPRamping.DISA 0") # Enable SPRamping

pvlistFromInfo("ARCHIVE_THIS", "$(IOCNAME):ArchiverList")
pvlistFromInfo("SAVRES_THIS", "$(IOCNAME):SavResList")
pvlistFromInfo("ARCHIVEWAVE_THIS", "$(IOCNAME):ArchiverWavList")
