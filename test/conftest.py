import sys
#TODO move to m-epics-sis8300llrf
sys.path.insert(1, '../../e3-sis8300llrf/sis8300llrf/test')

import pytest
from helper import get_mods_vers, BASE_BOARD


def pytest_addoption(parser):
    parser.addoption("--prefix", required="True")
    parser.addoption("--board", required="True")
    parser.addoption("--prefixdig")
    parser.addoption("--ssh") # ssh hostname

def pytest_generate_tests(metafunc):
    if "prefix" in metafunc.fixturenames:
        p = metafunc.config.getoption("prefix")
        if p is not None:
            metafunc.parametrize("prefix", [p])

    if "board" in metafunc.fixturenames:
        b = metafunc.config.getoption("board")
        if b is not None:
            metafunc.parametrize("board", [BASE_BOARD + '-' + b])

    if "prefixdig" in metafunc.fixturenames:
        p = metafunc.config.getoption("prefix")
        pd = metafunc.config.getoption("prefixdig")
        if pd is not None:
            metafunc.parametrize("prefixdig", [pd])
        else:
            metafunc.parametrize("prefixdig", [p + PREFIXDIG])

    if "ssh" in metafunc.fixturenames:
        s = metafunc.config.getoption("ssh")
        if s is not None:
            metafunc.parametrize("ssh", [s])
        else:
            metafunc.parametrize("ssh", [""])

def pytest_report_header(config):
    return get_mods_vers()
