from random import randint, random, uniform
from math import pi, sin, cos, atan2, sqrt
from time import sleep
import sys

import pytest
from epics import ca

# use helper from sis8300llrf module
sys.path.insert(1, '../../e3-sis8300llrf/sis8300llrf/test')
from helper import check_readback, change_state, sim_bp_trig, caget_assert, \
                   caput_assert, fixed_to_float, float_to_fixed
import numpy as np

PVFIXRAMPSTR="%s:%sTblRampStr-%s"
PVFIXRAMPEND="%s:%sTblRampEnd-%s"
PVFIXRAMPNELM="%s:%sTblRampSmpNm-%s"
PVMAG = "%s:%sTbl-Mag"
PVANG = "%s:%sTbl-Ang"
PVMAGRB = "%s:%sTbl-Mag-RB"
PVANGRB = "%s:%sTbl-Ang-RB"
PVIRB = "%s:%sTbl-I-RB"
PVQRB = "%s:%sTbl-Q-RB"
PVWRTTBL = "%s:%sTbl-TblToFW"
PVMAGCNST ="%s:RFCtrlCnst%s-Mag-RB"
PVANGCAL="%s:%sCal-Ang"

PVEGU = "%s:RFCtrl%sCalEGU"
PVRAW = "%s:RFCtrl%sCalRaw"

# PVs for autocalibration
PVNEWEGUCAV = "%s:%sCalNewEGUCav"
PVNEWEGUNOCAV = "%s:%sCalNewEGUNoCav"
PVNEWRAWCAV = "%s:%sCalNewRawCav"
PVNEWRAWNOCAV = "%s:%sCalNewRawNoCav"
PVCALSTAT = "%s:RFCtrl%sCalStat"

## Open loop PVs
PVCALIBTBLOPENCAVEGU="%s:%sCalResEGUCav"
PVCALIBTBLOPENNOCAVEGU="%s:%sCalResEGUNoCav"
PVCALIBTBLOPENCAVRAW="%s:%sCalResRawCav"
PVCALIBTBLOPENNOCAVRAW="%s:%sCalResRawNoCav"


PVAUTOEN="%s:%sAutoCalEn"
PVAUTONM="%s:%sAutoCalSmpNm"
PVAUTOSTR="%s:%sAutoCalStr"
PVAUTOEND="%s:%sAutoCalEnd"
PVSTATS="%s:%sCalStats"
PVSTATSLEN="%s:%sCalStatsLstVals.NORD"
PVSTATSSMPNM="%s:%sCalStatsSmpNm"
PVSTATSLSTCAV="%s:%sCavCalStatsLst"
PVSTATSLSTNOCAV="%s:%sNoCavCalStatsLst"
PVSTATSNEWVALCAV="%s:%sCavCalStatsNew"
PVSTATSNEWVALNOCAV="%s:%sNoCavCalStatsNew"

PVTBLMAGRB="%s:%sTbl-Mag-RB"
PVTBLANGRB="%s:%sTbl-Ang-RB"

# PVs for cavity and open loop
PVCAVITYEN="%s:CavityEn"
PVOPENLOOP="%s:OpenLoop"

# LIMITS
MAGLIMITS = [0, 1]
ANGLIMITS = [-pi, pi] # for SP and FF

PREC = 8

QMNIQ = (1, 15, 1)

def iq2magang(I, Q):
    mag = sqrt(I**2 + Q**2)
    ang = atan2(Q, I)

    return (mag, ang)

def magang2iq(mag, ang):
    I = mag*cos(ang)
    Q = mag*sin(ang)

    return (I, Q)

class TestPICalibration:
    """Test inserting one value per time on calibration tables
    Using linear calibration"""
    @pytest.mark.parametrize("pitype", ["SP"])
    def test_linear_new_val(self, prefix, prefixdig, pitype):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)


        # Disable calibration
        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEn", 0)
        # set linear
        caput_assert(prefixdig+":RFCtrl" + pitype + "CalLin", 1)

        # clean calibration tables
        caput_assert(prefix+":" + pitype + "CalRawRstCav", 1)
        caput_assert(prefix+":" + pitype + "CalEGURstCav", 1)

        caput_assert(prefix+":" + pitype + "CalRawRstNoCav", 1)
        caput_assert(prefix+":" + pitype + "CalEGURstNoCav", 1)

        assert caget_assert(PVCALIBTBLOPENCAVEGU %(prefix, pitype) + ".NORD") == 0
        assert caget_assert(PVCALIBTBLOPENNOCAVEGU %(prefix, pitype) + ".NORD") == 0
        assert caget_assert(PVCALIBTBLOPENCAVRAW %(prefix, pitype) + ".NORD") == 0
        assert caget_assert(PVCALIBTBLOPENNOCAVRAW %(prefix, pitype) + ".NORD") == 0

        # generate RAW and EGU arrays
        nelem = randint(20,1000)

        slope_cav = uniform(2, 100)
        offset_cav = uniform(2, 100)

        slope_nocav = uniform(2, 100)
        offset_nocav = uniform(2, 100)

        print("Slope Cav", slope_cav, "Offset ", offset_cav)
        print("Slope No Cav", slope_nocav, "Offset ", offset_nocav)

        raw_cav = []
        raw_nocav = []
        for i in range(nelem):
            raw_cav.append(random())
            raw_nocav.append(random())

        raw_cav = sorted(raw_cav)
        raw_nocav = sorted(raw_nocav)

        egu_cav = []
        egu_nocav = []

        for i in range(nelem):
            egu_cav.append(raw_cav[i]*slope_cav + offset_cav)
            egu_nocav.append(raw_nocav[i]*slope_nocav + offset_nocav)
            caput_assert(PVNEWEGUCAV % (prefix, pitype), egu_cav[i])
            caput_assert(PVNEWEGUNOCAV % (prefix, pitype), egu_nocav[i])
            caput_assert(PVNEWRAWCAV % (prefix, pitype), raw_cav[i])
            caput_assert(PVNEWRAWNOCAV % (prefix, pitype), raw_nocav[i])
            ca.poll(0.01)
            # Process the insertion
            caput_assert(prefix+":#" + pitype + "CalNewValCav.PROC", 1)
            caput_assert(prefix+":#" + pitype + "CalNewValNoCav.PROC", 1)

        # check with cavity PVs
        ca.poll(0.01)
        assert np.array_equal(caget_assert(PVCALIBTBLOPENCAVEGU  % (prefix, pitype)), egu_cav)
        assert np.array_equal(caget_assert(PVCALIBTBLOPENCAVRAW  % (prefix, pitype)), raw_cav)

        # check without cavity PVs
        ca.poll(0.01)
        assert np.array_equal(caget_assert(PVCALIBTBLOPENNOCAVEGU  % (prefix, pitype)), egu_nocav)
        assert np.array_equal(caget_assert(PVCALIBTBLOPENNOCAVRAW  % (prefix, pitype)), raw_nocav)

        ## Set openloop / cavity to tables be copied to the final destination
        # Set open loop (automatic calibration is only used when in open loop)
        check_readback(PVOPENLOOP % (prefixdig), 0)
        check_readback(PVOPENLOOP % (prefixdig), 1)
        # first test with cavity
        caput_assert(PVCAVITYEN % (prefix), 0)
        caput_assert(PVCAVITYEN % (prefix), 1)

        ## Check if the tables were correctly copied
        ca.poll(0.5)
        assert np.array_equal(caget_assert(PVEGU  % (prefixdig, pitype)), egu_cav)
        assert np.array_equal(caget_assert(PVRAW  % (prefixdig, pitype)), raw_cav)


        # Check if the calibration didn't generate any error
        assert caget_assert(prefixdig+":RFCtrl" + pitype + "CalStat") == 0

        # Check if a constant value is calibrated correctly , using without cavity
        max_val = egu_cav[-1]
        min_val = egu_cav[0]

        ca.poll(0.1)

        mag_val = uniform(min_val, max_val)
        ang_val = uniform(-pi, pi)
        mag_raw_val = (mag_val - offset_cav)/slope_cav

        # Enable calibration
        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEn", 1)
        ca.poll(0.1)

        assert check_readback(prefixdig+":RFCtrlCnst" + pitype + "-Mag", mag_val)
        assert check_readback(prefixdig+":RFCtrlCnst" + pitype + "-Ang", ang_val)

        I, Q = magang2iq(mag_raw_val, ang_val)

        ca.poll(0.1)
        I_rbv = caget_assert(prefixdig+":RFCtrlCnst" + pitype + "-I-RB")
        Q_rbv = caget_assert(prefixdig+":RFCtrlCnst" + pitype + "-Q-RB")

        assert abs(I - I_rbv) < 10**(-1*(4))
        assert abs(Q - Q_rbv) < 10**(-1*(4))

    """Test inserting one value per time on calibration tables
    Using non-linear calibration"""
    @pytest.mark.parametrize("pitype", ["SP"])
    def test_non_linear_new_val(self, prefix, prefixdig, pitype):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        # Enable calibration
        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEn", 0)
        # set non-linear
        caput_assert(prefixdig+":RFCtrl" + pitype + "CalLin", 0)

        # clean calibration tables
        caput_assert(prefix+":" + pitype + "CalRawRstCav", 1)
        caput_assert(prefix+":" + pitype + "CalEGURstCav", 1)

        caput_assert(prefix+":" + pitype + "CalRawRstNoCav", 1)
        caput_assert(prefix+":" + pitype + "CalEGURstNoCav", 1)

        # generate RAW and EGU arrays
        nelem = randint(20,1000)

        raw = []
        egu = []
        for i in range(nelem):
            raw.append(random())
            egu.append(random())

        raw = sorted(raw)
        egu = sorted(egu)

        for i in range(nelem):
            caput_assert(PVNEWEGUCAV % (prefix, pitype), egu[i])
            caput_assert(PVNEWRAWCAV % (prefix, pitype), raw[i])
            ca.poll(0.01)
            # Process the insertion
            caput_assert(prefix+":#" + pitype + "CalNewValCav.PROC", 1)

        ca.poll(0.1)
        ## Set openloop / cavity to tables be copied to the final destination
        # Set open loop (automatic calibration is only used when in open loop)
        check_readback(PVOPENLOOP % (prefixdig), 0)
        check_readback(PVOPENLOOP % (prefixdig), 1)
        #  with cavity
        caput_assert(PVCAVITYEN % (prefix), 0)
        caput_assert(PVCAVITYEN % (prefix), 1)

        ca.poll(0.1)
        assert np.array_equal(caget_assert(PVEGU  % (prefixdig, pitype)), egu)
        assert np.array_equal(caget_assert(PVRAW  % (prefixdig, pitype)), raw)

        # Check if the calibration didn't generate any error
        assert caget_assert(prefixdig+":RFCtrl" + pitype + "CalStat") == 0

        max_val = egu[-1]
        min_val = egu[0]

        ca.poll(0.1)

        mag_val = round(uniform(min_val, max_val), 10)
        ang_val = round(uniform(-pi, pi), 10)

        # Enable calibration
        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEn", 1)
        ca.poll(0.1)

        print(mag_val, ang_val)
        assert check_readback(prefixdig+":RFCtrlCnst" + pitype + "-Mag", mag_val)
        assert check_readback(prefixdig+":RFCtrlCnst" + pitype + "-Ang", ang_val)

    """Check if non sorted value works correcly on calibration"""
    @pytest.mark.parametrize("pitype", ["SP"])
    def test_new_val_not_sorted(self, prefix, prefixdig, pitype):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        # Enable calibration
        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEn", 0)
        # set linear
        caput_assert(prefixdig+":RFCtrl" + pitype + "CalLin", 1)


        # clean calibration tables
        caput_assert(prefix+":" + pitype + "CalRawRstCav", 1)
        caput_assert(prefix+":" + pitype + "CalEGURstCav", 1)

        caput_assert(prefix+":" + pitype + "CalRawRstNoCav", 1)
        caput_assert(prefix+":" + pitype + "CalEGURstNoCav", 1)

        # generate RAW and EGU arrays
        nelem = randint(20, 1000)
        print("nelm ", nelem)
        slope = uniform(2, 100)
        offset = uniform(2, 100)

        print("Slope ", slope, "Offset ", offset)

        raw = []
        for i in range(nelem):
            raw.append(random())

        raw = sorted(raw)

        egu = []

        for i in range(nelem-1):
            egu.append(raw[i]*slope + offset)
            caput_assert(PVNEWEGUCAV % (prefix, pitype), egu[i])
            caput_assert(PVNEWRAWCAV % (prefix, pitype), raw[i])
            ca.poll(0.01)
            # Process the insertion
            caput_assert(prefix+":#" + pitype + "CalNewValCav.PROC", 1)


        ca.poll(1)
        len_egu = len(caget_assert(PVCALIBTBLOPENCAVEGU  % (prefix, pitype)))
        len_raw = len(caget_assert(PVCALIBTBLOPENCAVRAW  % (prefix, pitype)))
        assert len_egu == len_raw and len_egu == nelem-1

        # test inserting both values wrong
        caput_assert(PVNEWEGUCAV % (prefix, pitype), egu[0])# try to insert the lower
        caput_assert(PVNEWRAWCAV % (prefix, pitype), raw[0])# try to insert the lower

        caput_assert(prefix+":#" + pitype + "CalNewValCav.PROC", 1)
        ca.poll(1)

        print(egu, raw)
        len_egu = len(caget_assert(PVCALIBTBLOPENCAVEGU  % (prefix, pitype)))
        len_raw = len(caget_assert(PVCALIBTBLOPENCAVRAW  % (prefix, pitype)))
        assert len_egu == len_raw and len_egu == nelem-1

        # test inserting EGU right and RAW wrong
        egu.append(raw[i+1]*slope + offset)
        caput_assert(PVNEWEGUCAV % (prefix, pitype), egu[i+1])# insert the last
        caput_assert(PVNEWRAWCAV % (prefix, pitype), raw[0])# try to insert the lower
        caput_assert(prefix+":#" + pitype + "CalNewValCav.PROC", 1)

        ca.poll(1)
        len_egu = len(caget_assert(PVCALIBTBLOPENCAVEGU  % (prefix, pitype)))
        len_raw = len(caget_assert(PVCALIBTBLOPENCAVRAW  % (prefix, pitype)))
        assert len_egu == len_raw and len_egu == nelem-1


        # test inserting RAW right and EGU wrong
        caput_assert(PVNEWRAWCAV % (prefix, pitype), raw[i+1])# insert the last
        caput_assert(PVNEWEGUCAV % (prefix, pitype), egu[0])# try to insert the lower
        caput_assert(prefix+":#" + pitype + "CalNewValCav.PROC", 1)
        ca.poll(1)
        len_egu = len(caget_assert(PVCALIBTBLOPENCAVEGU  % (prefix, pitype)))
        len_raw = len(caget_assert(PVCALIBTBLOPENCAVRAW  % (prefix, pitype)))
        assert len_egu == len_raw and len_egu == nelem-1

        # insert the last element correctly
        caput_assert(PVNEWEGUCAV % (prefix, pitype), egu[-1])# insert the last
        caput_assert(PVNEWRAWCAV % (prefix, pitype), raw[-1])# insert the last
        ca.poll(0.01)
        caput_assert(prefix+":#" + pitype + "CalNewValCav.PROC", 1)
        ca.poll(0.01)
        len_egu = len(caget_assert(PVCALIBTBLOPENCAVEGU  % (prefix, pitype)))
        len_raw = len(caget_assert(PVCALIBTBLOPENCAVRAW  % (prefix, pitype)))
        print(egu, raw)
        assert len_egu == len_raw and len_egu == nelem

    """Test statistics """
    @pytest.mark.parametrize("pitype", ["SP"])
    @pytest.mark.parametrize("cav", ["Cav", "NoCav"])
    def test_statistics(self, prefix, pitype, cav):
        caput_assert(prefix + ":" + pitype + "CalStatsRst", 1)
        # Enable
        caput_assert(prefix + ":" + pitype + "CalStatsEn", 1)

        #Un link New val
        caput_assert(prefix + ":" + pitype + cav + "CalStatsNew.INP", "")

        winsize = random()
        npoints = randint(10,1000)
        caput_assert(prefix + ":" + pitype + "CalStatsWinSize", winsize)
        caput_assert(prefix + ":" + pitype + "CalStatsSmpNm", npoints)

        points = []
        for i in range(npoints):
            points.append(random())
            caput_assert(prefix + ":" + pitype + cav + "CalStatsNew",points[i])
            # on real case this will be processed by the pulse
            caput_assert(prefix + ":#" + pitype + cav + "CalStats.PROC",1)
            ca.poll(0.01)
        points = sorted(points)

        points_rbv = caget_assert(prefix + ":" + pitype + cav + "CalStatsLst")
        assert np.array_equal(points, points_rbv)

        result = caget_assert(prefix + ":" + pitype + cav + "CalStats")

        # check result
        median = np.median(points)
        stats = []
        for j in range(npoints):
            if points[j] >= median*(1 - winsize) and points[j] <= median*(1 + winsize):
                stats.append(points[j])

        avg_stats = 0
        if len(stats) != 0:
            avg_stats = np.average(stats)
        assert abs(avg_stats - result) < 10**-12

    @pytest.mark.parametrize("pitype", ["SP"])
    def test_simulate_auto_calibration_cav(self, prefix, prefixdig, board, ssh, pitype, calib_size = 5):
        for state in ["RESET", "INIT", "ON"]:
            change_state(prefixdig, state)

        ## Set openloop / cavity to tables be copied to the final destination
        # Set open loop (automatic calibration is only used when in open loop)
        check_readback(PVOPENLOOP % (prefixdig), 0)
        check_readback(PVOPENLOOP % (prefixdig), 1)
        #  with cavity
        caput_assert(PVCAVITYEN % (prefix), 0)
        caput_assert(PVCAVITYEN % (prefix), 1)

        #disable autocalib
        caput_assert(PVAUTOEN % (prefix, pitype), 0)

        # Unlink the New Val
        caput_assert(prefix + ":" + pitype + "CavCalStatsNew.INP", "")

        stats_smpnm = randint(2, 10)
        points_nm = randint(2, 10)
        start = round(uniform(MAGLIMITS[0], MAGLIMITS[1]/2), 12)
        end = round(uniform(MAGLIMITS[1]/2, MAGLIMITS[1]), 12)
        step = round((end - start)/points_nm, 12)

        cur_mag = start


        winsize = uniform(0.1,1)
        caput_assert(prefix + ":" + pitype + "CalStatsWinSize", winsize)

        caput_assert(PVSTATSSMPNM % (prefix, pitype), stats_smpnm)
        caput_assert(PVAUTONM % (prefix, pitype), points_nm)
        caput_assert(PVAUTOSTR % (prefix, pitype), start)
        caput_assert(PVAUTOEND % (prefix, pitype), end)

        sleep(0.1)
        caput_assert(PVAUTOEN % (prefix, pitype), 1)
        # write 0 to angle
        caput_assert(PVANGCAL % (prefix, pitype), 0)

        total_points = points_nm

        sim_bp_trig(board, ssh)
        for j in range(points_nm):
            print("Size of EGU table : ", caget_assert(PVCALIBTBLOPENCAVEGU % (prefix, pitype) + ".NORD"))

            i_conv, q_conv =  magang2iq(cur_mag, 0)
            mag_cur, _ = iq2magang(i_conv,q_conv)
            assert abs(caget_assert(PVMAGCNST % (prefixdig, pitype))- mag_cur) < 10**-7

            cur_mag += step
            print("Cur mag, step, end ", cur_mag, step, end)
            print("Cur mag RB ", caget_assert(PVMAGCNST % (prefixdig, pitype)))

            # There are cases where the last point is not added
            if (j == points_nm - 1) and caget_assert(PVAUTOEN % (prefix, pitype)) == 0:
                total_points = caget_assert(PVCALIBTBLOPENCAVEGU % (prefix, pitype) + ".NORD")
                continue

            # auto calib should be on
            assert caget_assert(PVAUTOEN % (prefix, pitype)) == 1

            for i in range(stats_smpnm):
                #add a simulated point
                caput_assert(PVSTATSNEWVALCAV % (prefix, pitype), (j+1)*(i+1)*0.1)
                sim_bp_trig(board, ssh)
                print("Stats last: ", caget_assert(PVSTATSLSTCAV % (prefix, pitype) ))

            sleep(1)


            # print new egu new raw
            print("NEWEGU ", caget_assert(PVNEWEGUCAV % (prefix, pitype)))
            print("NEWRAW ", caget_assert(PVNEWRAWCAV % (prefix, pitype)))
            # print egu and raw
            print("EGU ", caget_assert(PVCALIBTBLOPENCAVEGU % (prefix, pitype)))
            print("RAW ", caget_assert(PVCALIBTBLOPENCAVRAW % (prefix, pitype)))

            assert caget_assert(PVCALIBTBLOPENCAVEGU % (prefix, pitype) + ".NORD") == j + 1

            # trigger to apply magnitude value
            sim_bp_trig(board, ssh)

            sleep(1)
            # check ramp mode?
            # update table readbacks
#            caput_assert(PVTBLMAGRB % (prefixdig, pitype)+".PROC", 1)
#            caput_assert(PVTBLANGRB % (prefixdig, pitype)+".PROC", 1)

        # auto-calib should be false
        assert caget_assert(PVAUTOEN % (prefix, pitype)) == 0
        # status from calibration table should be ok
        if caget_assert(PVCALIBTBLOPENCAVEGU % (prefix, pitype) + ".NORD") > 1:
            assert caget_assert(PVCALSTAT % (prefixdig, pitype)) == 0

            assert(total_points == caget_assert(PVEGU % (prefixdig, pitype) + ".NORD"))
            assert(total_points == caget_assert(PVRAW % (prefixdig, pitype) + ".NORD"))


    @pytest.mark.parametrize("pitype", ["SP"])
    def test_simulate_auto_calibration_nocav(self, prefix, prefixdig, board, ssh, pitype, calib_size = 5):
        for state in ["RESET", "INIT", "ON"]:
            change_state(prefixdig, state)

        ## Set openloop / cavity to tables be copied to the final destination
        # Set open loop (automatic calibration is only used when in open loop)
        check_readback(PVOPENLOOP % (prefixdig), 0)
        check_readback(PVOPENLOOP % (prefixdig), 1)
        #  without cavity
        caput_assert(PVCAVITYEN % (prefix), 1)
        caput_assert(PVCAVITYEN % (prefix), 0)

        #disable autocalib
        caput_assert(PVAUTOEN % (prefix, pitype), 0)

        # Unlink the New Val
        caput_assert(prefix + ":" + pitype + "NoCavCalStatsNew.INP", "")

        stats_smpnm = randint(2, 10)
        points_nm = randint(2, 10)
        start = round(uniform(MAGLIMITS[0], MAGLIMITS[1]/2), 12)
        end = round(uniform(MAGLIMITS[1]/2, MAGLIMITS[1]), 12)
        step = round((end - start)/points_nm, 12)

        cur_mag = start


        winsize = uniform(0.1,1)
        caput_assert(prefix + ":" + pitype + "CalStatsWinSize", winsize)

        caput_assert(PVSTATSSMPNM % (prefix, pitype), stats_smpnm)
        caput_assert(PVAUTONM % (prefix, pitype), points_nm)
        caput_assert(PVAUTOSTR % (prefix, pitype), start)
        caput_assert(PVAUTOEND % (prefix, pitype), end)

        sleep(0.1)
        caput_assert(PVAUTOEN % (prefix, pitype), 1)
        # write 0 to angle
        caput_assert(PVANGCAL % (prefix, pitype), 0)

        total_points = points_nm

        sim_bp_trig(board, ssh)
        for j in range(points_nm):
            print("Size of EGU table : ", caget_assert(PVCALIBTBLOPENNOCAVEGU % (prefix, pitype) + ".NORD"))

            i_conv, q_conv =  magang2iq(cur_mag, 0)
            mag_cur, _ = iq2magang(i_conv,q_conv)
            assert abs(caget_assert(PVMAGCNST % (prefixdig, pitype))- mag_cur) < 10**-7

            cur_mag += step
            print("Cur mag, step, end ", cur_mag, step, end)
            print("Cur mag RB ", caget_assert(PVMAGCNST % (prefixdig, pitype)))

            # There are cases where the last point is not added
            if (j == points_nm - 1) and caget_assert(PVAUTOEN % (prefix, pitype)) == 0:
                total_points = caget_assert(PVCALIBTBLOPENNOCAVEGU % (prefix, pitype) + ".NORD")
                continue

            # auto calib should be on
            assert caget_assert(PVAUTOEN % (prefix, pitype)) == 1

            for i in range(stats_smpnm):
                #add a simulated point
                caput_assert(PVSTATSNEWVALNOCAV % (prefix, pitype), (j+1)*(i+1)*0.1)
                sim_bp_trig(board, ssh)
                print("Stats last: ", caget_assert(PVSTATSLSTNOCAV % (prefix, pitype) ))

            sleep(1)


            # print new egu new raw
            print("NEWEGU ", caget_assert(PVNEWEGUNOCAV % (prefix, pitype)))
            print("NEWRAW ", caget_assert(PVNEWRAWNOCAV % (prefix, pitype)))
            # print egu and raw
            print("EGU ", caget_assert(PVCALIBTBLOPENNOCAVEGU % (prefix, pitype)))
            print("RAW ", caget_assert(PVCALIBTBLOPENNOCAVRAW % (prefix, pitype)))

            assert caget_assert(PVCALIBTBLOPENNOCAVEGU % (prefix, pitype) + ".NORD") == j + 1

            # trigger to apply magnitude value
            sim_bp_trig(board, ssh)

            sleep(1)
            # check ramp mode?
            # update table readbacks
#            caput_assert(PVTBLMAGRB % (prefixdig, pitype)+".PROC", 1)
#            caput_assert(PVTBLANGRB % (prefixdig, pitype)+".PROC", 1)

        # auto-calib should be false
        assert caget_assert(PVAUTOEN % (prefix, pitype)) == 0
        # status from calibration table should be ok
        if caget_assert(PVCALIBTBLOPENNOCAVEGU % (prefix, pitype) + ".NORD") > 1:
            assert caget_assert(PVCALSTAT % (prefixdig, pitype)) == 0

            assert(total_points == caget_assert(PVEGU % (prefixdig, pitype) + ".NORD"))
            assert(total_points == caget_assert(PVRAW % (prefixdig, pitype) + ".NORD"))


    @pytest.mark.parametrize("pitype", ["SP"])
    def test_table_ramp(self, prefix, prefixdig, pitype):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        # Disable calibration
        caput_assert(prefixdig+":RFCtrl" + pitype + "CalEn", 0)

        mag_ang_len = randint(10, 1000)

        mag_start = uniform(MAGLIMITS[0], MAGLIMITS[1]/10)
        ang_start = uniform(MAGLIMITS[0], MAGLIMITS[1]/10)

        mag_end = uniform(mag_start, MAGLIMITS[1])
        ang_end = uniform(ang_start, ANGLIMITS[1])

        caput_assert(PVFIXRAMPNELM % (prefix, pitype, "Mag"), mag_ang_len)
        caput_assert(PVFIXRAMPNELM % (prefix, pitype, "Ang"), mag_ang_len)

        caput_assert(PVFIXRAMPSTR % (prefix, pitype, "Mag"), mag_start)
        caput_assert(PVFIXRAMPSTR % (prefix, pitype, "Ang"), ang_start)

        caput_assert(PVFIXRAMPEND % (prefix, pitype, "Mag"), mag_end)
        caput_assert(PVFIXRAMPEND % (prefix, pitype, "Ang"), ang_end)

        mag_tab = caget_assert(PVMAG % (prefixdig, pitype))
        ang_tab = caget_assert(PVANG % (prefixdig, pitype))

        mag_exp = np.linspace(mag_start, mag_end, mag_ang_len)
        ang_exp = np.linspace(ang_start, ang_end, mag_ang_len)

        assert np.array_equal(np.round(mag_exp, PREC), np.round(mag_tab, PREC))
        assert np.array_equal(np.round(ang_exp,PREC), np.round(ang_tab, PREC))

        # write tables
        caput_assert(PVWRTTBL % (prefixdig, pitype), 1)
        # update readback from mag/ang and I/Q
        caput_assert(PVMAGRB % (prefixdig, pitype) + ".PROC", 1)
        caput_assert(PVANGRB % (prefixdig, pitype) + ".PROC", 1)
        caput_assert(PVIRB % (prefixdig, pitype) + ".PROC", 1)
        caput_assert(PVQRB % (prefixdig, pitype) + ".PROC", 1)

        sleep(0.1)
        i_rbv = caget_assert(PVIRB % (prefixdig, pitype))
        q_rbv = caget_assert(PVQRB % (prefixdig, pitype))
        mag_rbv = caget_assert(PVMAGRB % (prefixdig, pitype))
        ang_rbv = caget_assert(PVANGRB % (prefixdig, pitype))

        for pos in range(mag_ang_len):
            i, q = magang2iq(mag_exp[pos], ang_exp[pos])
            i = fixed_to_float(float_to_fixed(i, *QMNIQ),*QMNIQ)
            q = fixed_to_float(float_to_fixed(q, *QMNIQ),*QMNIQ)
            mag_cur, ang_cur = iq2magang(i, q)

            assert i == i_rbv[pos]
            assert q == q_rbv[pos]
            assert mag_cur  == mag_rbv[pos]
            assert ang_cur  == ang_rbv[pos]
