## Tests the auto-choose for PI calibration
from random import randint
from math import pi
from time import sleep
import sys

import numpy as np
import pytest

# use helper from sis8300llrf module
sys.path.insert(1, '../../e3-sis8300llrf/sis8300llrf/test')
from helper import check_readback, change_state, caget_assert, \
                   caput_assert

TBLMAXSIZE=1000

# Open loop PVs
PVCALIBTBLOPENCAVEGU="%s:%sCalResEGUCav"
PVCALIBTBLOPENNOCAVEGU="%s:%sCalResEGUNoCav"
PVCALIBTBLOPENCAVRAW="%s:%sCalResRawCav"
PVCALIBTBLOPENNOCAVRAW="%s:%sCalResRawNoCav"

# Closed loop PVs
PVCALIBTBLCLOSECAVEGU="%s:AI0-CalEGU"
PVCALIBTBLCLOSECAVRAW="%s:AI0-CalRaw"
PVCALIBTBLCLOSENOCAVEGU="%s:AI3-CalEGU"
PVCALIBTBLCLOSENOCAVRAW="%s:AI3-CalRaw"

# PVs for cavity and open loop
PVCAVITYEN="%s:CavityEn"
PVOPENLOOP="%s:OpenLoop"

# PI Calibration tables
PVPICALIBTBLEGU="%s:RFCtrl%sCalEGU"
PVPICALIBTBLRAW="%s:RFCtrl%sCalRaw"

MAGLIMITS = [0, 1]
ANGLIMITS = [-pi, pi] # for SP and FF

EGULIMITS = [0, 1500]

"""
Set random values for a calibration table
"""
def gen_calib_tbl(size, limits):
    return sorted((limits[1]-limits[0]) * np.random.rand(size) + limits[0])

class TestPICalibrationAutoChoose:
    @pytest.mark.parametrize("pitype", ["SP"])
    def test_auto_choose(self, prefix, prefixdig, pitype):
        for state in ["RESET", "INIT"]:
            assert change_state(prefixdig, state)

        # set values for open loop pvs
        maxi = randint(100, TBLMAXSIZE)
        caput_assert(PVCALIBTBLOPENCAVEGU % (prefix, pitype), gen_calib_tbl(maxi, EGULIMITS))
        caput_assert(PVCALIBTBLOPENCAVRAW % (prefix, pitype), gen_calib_tbl(maxi, MAGLIMITS))
        maxi = randint(100, TBLMAXSIZE)
        caput_assert(PVCALIBTBLOPENNOCAVEGU % (prefix, pitype), gen_calib_tbl(maxi, EGULIMITS))
        caput_assert(PVCALIBTBLOPENNOCAVRAW % (prefix, pitype), gen_calib_tbl(maxi, MAGLIMITS))

        # set values for closed loop pvs
        maxi = randint(100, TBLMAXSIZE)
        caput_assert(PVCALIBTBLCLOSECAVEGU % (prefixdig), gen_calib_tbl(maxi, EGULIMITS))
        caput_assert(PVCALIBTBLCLOSECAVRAW % (prefixdig), gen_calib_tbl(maxi, MAGLIMITS))
        maxi = randint(100, TBLMAXSIZE)
        caput_assert(PVCALIBTBLCLOSENOCAVEGU % (prefixdig), gen_calib_tbl(maxi, EGULIMITS))
        caput_assert(PVCALIBTBLCLOSENOCAVRAW % (prefixdig), gen_calib_tbl(maxi, MAGLIMITS))

        # "Reset" Open loop / Cavity
        check_readback(PVOPENLOOP % (prefixdig), 0)
        caput_assert(PVCAVITYEN % (prefix), 0)
        check_readback(PVOPENLOOP % (prefixdig), 1)
        caput_assert(PVCAVITYEN % (prefix), 1)

        #  1st case - Open Loop / without cavity
        check_readback(PVOPENLOOP % (prefixdig), 1)
        caput_assert(PVCAVITYEN % (prefix), 0)
        sleep(1)

        assert np.array_equal(caget_assert(PVPICALIBTBLEGU % (prefixdig, pitype)), caget_assert(PVCALIBTBLOPENNOCAVEGU % (prefix, pitype)))
        assert np.array_equal(caget_assert(PVPICALIBTBLRAW % (prefixdig, pitype)), caget_assert(PVCALIBTBLOPENNOCAVRAW % (prefix, pitype)))

        #  2st case - Open Loop / with cavity
        check_readback(PVOPENLOOP % (prefixdig), 1)
        caput_assert(PVCAVITYEN % (prefix), 1)
        sleep(1)

        assert np.array_equal(caget_assert(PVPICALIBTBLEGU % (prefixdig, pitype)), caget_assert(PVCALIBTBLOPENCAVEGU % (prefix, pitype)))
        assert np.array_equal(caget_assert(PVPICALIBTBLRAW % (prefixdig, pitype)), caget_assert(PVCALIBTBLOPENCAVRAW % (prefix, pitype)))


        #  3rd case - Closed Loop / without cavity
        check_readback(PVOPENLOOP % (prefixdig), 0)
        caput_assert(PVCAVITYEN % (prefix), 0)
        sleep(1)

        assert np.array_equal(caget_assert(PVPICALIBTBLEGU % (prefixdig, pitype)), caget_assert(PVCALIBTBLCLOSENOCAVEGU % (prefixdig)))
        assert np.array_equal(caget_assert(PVPICALIBTBLRAW % (prefixdig, pitype)), caget_assert(PVCALIBTBLCLOSENOCAVRAW % (prefixdig)))

        #  4th case - Closed Loop / with cavity
        check_readback(PVOPENLOOP % (prefixdig), 0)
        caput_assert(PVCAVITYEN % (prefix), 1)
        sleep(1)

        assert np.array_equal(caget_assert(PVPICALIBTBLEGU % (prefixdig, pitype)), caget_assert(PVCALIBTBLCLOSECAVEGU % (prefixdig)))
        assert np.array_equal(caget_assert(PVPICALIBTBLRAW % (prefixdig, pitype)), caget_assert(PVCALIBTBLCLOSECAVRAW % (prefixdig)))
