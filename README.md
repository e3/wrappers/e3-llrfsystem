
# e3-llrfsystem

ESS Site-specific EPICS module : llrfsystem

This module provides high level functionalities for a LLRF System.

## Snippets for different sections

On the iocsh folder a set of snippets for different sections (rfq, dtl, ...)
is provide to be used by the LLRF IOCs.

## System Parameters

Main parameters like Frequency Sampling and System Frequency

## Cavity De-tuning

Implemented using a aSub record.
This implementation covers only the NCL cavities.

## Feed Forward automatic calibration

Implemented using sequencer module, this calibration runs the LLRF from minimum
to the maximum value doing some statistics and saving the output from
power amplifier forward. The result is a table to scale Feed Forward values.

## Pulse Generation

Fills Feed Forward table with a right shape.

## Frequency Tracking

Set the right phase table for Feed Forward based on some source (like detuning)

## Set Point Ramping

Fills the Set Point table with the right shape

## Gains

Calculate gains from power amplifier and pre amplifier

## Select main digitizer

Used on systems where 2 digitizer can drive the output.

## Monitor Interlock

Check if there is a interlock on one of the boards to trigger the interlock
on other

## Kly Info

Stores klystron information
