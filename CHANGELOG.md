# 3.19.4
* Update beam reach calculation to include new destinations (ICSHWI-19889)
* Make FF phase follow SP phase when closed loop operation (ICSHWI-20240)

# 3.19.3
* Remove specific scl cavity record which was moved to sis8300llrf module

# 3.19.2
* Rename DTL cavity field channels for third digitizer (ICSHWI-18550)
* Fix setting RCAV macro for specific SCL record (ICSHWI-18563)

# 3.19.1
* Use iocname prefix for archiver and savRes list PVs (ICSHWI-17941)
* Fix condition when frequency tracking offset is disabled (after ICSHWI-10325)

# 3.19.0
* Implement automatic behaviors for LLRF Operation Modes (ICSHWI-12408)
* Disable Frequency tracking offset in specific scenarios (ICSHWI-10325)
* Changing SCL macro to SCL_TS2, Adding TS2 and SCL macros (part of ICSHI-14673)

# 3.18.4
* Add missing colon in R macro (fix invalid PV-s for RFQ)

# 3.18.3
* Restore loading detune records (fix after ICSHWI-12558)

# 3.18.2
* Fix processing units for FF and SP (ICSHWI-15574)

# 3.18.1
* Add macros to specify custom values for frequency sampling and near IQ for tests

# 3.18.0
* Set correct units for FF and SP power/cavity field (ICSHWI-12558)

# 3.17.1
* Fix passing default EGU for channel 0 in SCL (fix after ICSHWI-7957)

# 3.17.0
* Fix LLRF TS2 main instance not works always (ICSHWI-10084)

# 3.16.1
* Let rfq configuration adapted to be more standard (ICSHWI-13730)
* Remove unused macro (ICSHWI-13731)

# 3.16.0
* TS2 MTCA modules reordering request. The main digitizer position went from 4 to 3 and the second digitizer from position 6 to 4 (ICSHWI-13123)

# 3.15.3
* Fix syntax in dbd file (issue #2)

# 3.15.2
* Fix generic snippet to support changing the F-SYSTEM (ICSHWI-13217)

# 3.15.1
* Fix ILC to use internal reference for I and Q (ICSHWI-13143)

# 3.15.0
* Set engineer units as macros for channels (ICSHWI-7957)

# 3.14.3
* Small fix on Pulse Generator (fix data buffer PV)
* Small fixes on ILC (Fix how disable pulse gen and missing output PV on Calc-I)
* Simplify the copy of pulse generator output when start ILC

# 3.14.2
* Change time integration to consider beam start fixed, and beam end
depending on beam length

# 3.14.1
* Improve how the Pulse Generator and Frequency tracking are triggered
* Move remaining static compensation calculation to ILC (to not use the ones
from pulse generator)

# 3.14.0
* Include an Operation Mode PV
* Include a PV for RF Station number, to be used to check the beam destination
* Remove all timing integration from pulse-generator (static compensation)
* Create a new db with the timing integration (timing_integration.template), which decode all the used information from data buffer, and implements the decision on how the FF should be filled depending on how will be the next beam
* Change the ILC to have save the last output table, after a pulse without beam
* Include the automatic set of beam start and beam end for ILC (when in beam operation mode)
* Include on ILC mechanisms to decide when it should be processed, and if the produced output should be used or not
* Include a few debug PVs to check the LLRF behavior
* Implement static compensation also in ILC, so it can be used there when in beam operation mode
* Block ILC to be processed when an interlock happened last pulse
* Closes: ICSHWI-11164, ICSHWI-11415, ICSHWI-11498, ICSHWI-11416, ICSHWI-11388

# 3.13.4
* Include a PV for Qext (ICSHWI-12046)

# 3.13.3
* Include an option to set the PV for RF width (ICSHWI-11857)

# 3.13.2
* Fix cavity detuning calculation with phase in degrees

# 3.13.1
* Fix issues with ramp fall on Pulse Generator and SP Ramping

# 3.13.0
* Move all angle PVs to degrees

# 3.12.0
* Include the ramp fall on pulse generator and SP ramping
* Change FF auto calibration to use pulse generator
* Limit the tables from pulse generator and SP ramping to the RF length from timing

# 3.11.2
* Fix gain calculations
* Fix power forward channel definition for ff automatic calibration
* Remove load of inexistent file (ilc-pid.template)

# 3.11.1
* Remove temporary fields for derivative part

# 3.11.0
* Include automatic feedforward static compensation

# 3.10.1
* Remove klystron information from Spokes

# 3.10.0
* Generate a list of waveform PVs to be archived

# 3.9.0
* Update Setpoint ramping to work on SC cavities

# 3.8.2
* Fix small issue on ILC template
* Disable frequency tracking and SP ramping during IOC startup

# 3.8.1
* Set LPSEN to 1 as default on first digitizer

# 3.8.0
* Include archiver and save and restore info tags
* Include iocmetadata module to generate archiver and save and restore PVs

# 3.7.17
* Include SP Ramping PVs on autosave
* Set SP Ramping phase PREC to 9

# 3.7.16
* Fix internal mask used by ILC

# 3.7.15
* Make ILC uses FF I/Q Set Point and reload it automatically before starte

# 3.7.14
* Remove some warnings from c and c++ code
* Include pulse generator PVs on autosave

# 3.7.13
* Include PV to generate constant phase on SP table
* Include snippet for Spoke

# 3.7.12
* Increase callbackqueuesize to 100000

# 3.7.11
* Remove AS_TOP from base snippet

# 3.7.10
* Add Iterative Learning Control

# 3.7.9
* Update snippets to have a common base

# 3.7.8
* Include FF static compensation for phase
* Include a PV to enable/disable FF static compensation

# 3.7.7
* Disable Klystron PVs on TS2 secondary

# 3.7.6
* Set LPSEN enable as default for all systems

# 3.7.5
* Fix PREC for pulse gen fields

# 3.7.4
* Fix call to spramping on snippets

# 3.7.3
* Merge the FF static compensation and SP Ramping

# 3.7.2
* Fix cavity channels for MBLs (used for autocalibration)

# 3.7.1
* Fix FF autocalib

# 3.7.0
* Remove method do select calibration (it is done by sis8300llrf now)
* Remove CavityEn (the PV is now on sis8300llrf and it is CloseLoopCav)

# 3.6.4
* Include FF static compensation
* Include SP Ramping

# 3.6.3
* Fix frequency tracking signal
